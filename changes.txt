Desktop Version with ElectronJS
===============================

1.  Set isoffline = tf and manifest > eventTitle in environment file.

2.  Make build by npm run build

3.  After making build, edit index.html file from dist folder and change <base href="/"> to <base href="./">

5.  Copy all files and folders from dist folder to DesktopView folder and overwrite if already exists.

6.  In the DesktopView folder, run the npm command as ...
	To Setup: npm install
	To Run App: npm start
	To Make EXE: npm run build


Desktop Version with NWJS
=========================
1.  First make build of your project and copy the dist folder files into DesktopNWJS\dist folder. Do not remove the package.json file from dist folder.

2.  Under DesktopNWJS folder, run the following commands on commandline CMD.
	1.  npm install (first time)
	2.  grunt build (every time when making build)

3.  Now build folder is generated with all required files to run the Desktop version offline.