import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { SharedNotifyModule } from './custom-comp/notification/notify.component';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let el: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedNotifyModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    // create component and test fixture
    fixture = TestBed.createComponent(AppComponent);
    // get test component from the fixture
    component = fixture.componentInstance;
    // get the "h1" element by CSS selector
    el = fixture.debugElement.query(By.css('h1'));
  });

  it('should create the app', async(() => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));

  it(`should have as title 'ism'`, async(() => {
    // const fixture = TestBed.createComponent(AppComponent);
    // const app = fixture.debugElement.componentInstance;
    expect(component.title).toEqual('ism');
  }));

  // it('should render title in a h1 tag', async(() => {
  //   // const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   // const compiled = fixture.debugElement.nativeElement;
  //   expect(el.nativeElement.textContent.trim()).toContain('Welcome to ism!');
  // }));
});
