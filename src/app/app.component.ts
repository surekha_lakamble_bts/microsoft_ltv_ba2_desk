import { Component } from '@angular/core';

@Component({
  selector: 'ism-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent {
  title = 'ism';
}
