import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewsModule } from './views/views.module';
import { SharedModule } from './shared/shared.module';
import { ManifestService, HttpWrapperService } from '@btsdigital/ngx-isomer-core';
// import { ParticipantAppAuthoringModule } from '@btsdigital/pulseparticiapntappauthoring';
// import { NavBarComponent } from './custom-comp/nav-bar/nav-bar.component';
import { SignalRService } from '@btsdigital/pulsesignalr';
// import { LoggerService } from '@btsdigital/pulseutilities';
import { AppConfig } from './app.config';
// import { SharedModule } from './shared/shared.module';
// import { IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
// import { Ng2BootstrapModule } from 'ngx-bootstrap';
import { VideoCallManagerService } from '@btsdigital/pulsewebrtc';
import { ComponentsModule } from './components/components.module'

@NgModule({
  declarations: [
    AppComponent
    // NavBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // IsomerCoreModule,
    ViewsModule,
    BrowserAnimationsModule,
    // SharedModule,
    SharedModule, ComponentsModule
    // ParticipantAppAuthoringModule
    // Ng2BootstrapModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

