import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { AccordianComponent } from './accordian.component';
import { StorageService, IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

describe('AccordianComponent', () => {

  let component: AccordianComponent;
  let fixture: ComponentFixture<AccordianComponent>;
  let service: StorageService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IsomerCoreModule, RouterTestingModule],
      declarations: [AccordianComponent],
      providers: [StorageService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordianComponent);
    component = fixture.componentInstance;
    component.accorData = { 'titleRef': 'Sales and Marketing', 'icon': '', 'route': 'test', 'isOpen': true, 'accordianId': '05' };
    service = TestBed.get(StorageService);
    fixture.detectChanges();
  });

  afterEach(() => {
    localStorage.removeItem('accorStatus');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Call ngoninit with localstorage data', fakeAsync(() => {
    service.setValue('accorStatus', JSON.stringify([component.accorData]));

    component.ngOnInit();

    expect(component.accorData.isOpen).toBeTruthy();
    tick();
    expect(component.accorData.isOpen).toEqual(component.classStatus[0].isOpen);

  }));

  it('Call ngoninit without localstorage data', fakeAsync(() => {
    component.ngOnInit();
    tick();

    expect(component.accorData.isOpen).toBeTruthy();

  }));

  it('On click openAccordian() should call with localStorage', fakeAsync(() => {
    service.setValue('accorStatus', JSON.stringify([component.accorData]));

    const el = fixture.nativeElement.querySelector('.accor-icon');
    el.click();

    expect(component.accorData.isOpen).toBeFalsy();
    tick();
    expect(component.classStatus[0].accordianId).toBe('05');

  }));

  it('On click openAccordian() should call without localStorage', fakeAsync(() => {
    component.classStatus = [];

    const el2 = fixture.nativeElement.querySelector('.accor-icon');
    el2.click();

    expect(component.accorData.isOpen).toBeFalsy();
    tick();
    expect(component.classStatus[0].accordianId).toBe('05');
  }));

});
