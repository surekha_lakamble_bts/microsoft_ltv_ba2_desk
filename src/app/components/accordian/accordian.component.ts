import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { StorageService } from '@btsdigital/ngx-isomer-core';
import { NavBar } from '../../interfaces';
import * as _ from 'lodash';
/**
 * Accordian component renders the Accordian based on the provided input
 * Accordian is rendered using ism-accordian component of ngx-isomer-core
 *
 */
@Component({
  selector: 'ism-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.styl']
})
export class AccordianComponent implements OnInit {

  /**
   * Input binding for accorData which will render the Accordian
   *
   */
  @Input() accorData: NavBar;

  @Input() accorType: string;

  /**
   * Constructor for accordian component
   *
   * @param {StorageService} storage StorageService instance
   *
   */
  constructor(private storage: StorageService) { }

  /**
   * classStatus is for value for active state opened accordian
   */
  classStatus: any = [];

  /**
 * LifeCycle hook ngOnInit
 */
  ngOnInit() {
    this.storage.getValue('accorStatus').then((val) => {
      if (val) {
        this.classStatus = JSON.parse(val.toString());
      }
      this.checkAccordian();

    });

  }

  /**
   * checkAccordian is called on ngOninit load data from localstorage else assign input data
   */
  checkAccordian() {
    const index = _.findIndex(this.classStatus, { accordianId: this.accorData.accordianId });

    if (index > -1) {
      this.accorData.isOpen = this.classStatus[index].isOpen;
    } else {
      this.accorData.isOpen = this.accorData.isOpen ? true : false;
    }
  }

  /**
   * openAccordian is called on click toggle icon
   * @param value is for list of classes.
   */
  openAccordian(value: NavBar) {

    this.accorData.isOpen = !this.accorData.isOpen;

    this.storage.getValue('accorStatus').then((val) => {
      if (val) {
        this.classStatus = JSON.parse(val.toString());
      }
      const index = _.findIndex(this.classStatus, { accordianId: value.accordianId });
      if (index > -1) {
        this.classStatus[index].isOpen = this.accorData.isOpen;
      } else {
        this.classStatus.push(value);
      }
      this.storage.setValue('accorStatus', JSON.stringify(this.classStatus));
    });

  }

}
