/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CalcstepperComponent } from './calcstepper.component';

describe('CalcstepperComponent', () => {
  let component: CalcstepperComponent;
  let fixture: ComponentFixture<CalcstepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcstepperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcstepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
