import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { CalcStepperComponent as IsomerCalcStepperComponent, CalcService, CommunicatorService, NumberFormattingPipe  } from '@btsdigital/ngx-isomer-core';

@Component({
  selector: 'isma-calc-stepper',
  templateUrl: './calcstepper.component.html',
  styleUrls: ['./calcstepper.component.styl']
})
export class CalcStepperComponent extends IsomerCalcStepperComponent implements OnInit {
  @Input() outputRangeRef: string = '';
  @Input() outputFormat: string = '';
  @Input() outputScaler: number = 1;
  private outputRangeValue;
  private hasDifferentOutput: boolean = false; 

  // local declarations which are private in CalcInputComponent of IsomerCoreModule
  private localCalcService: CalcService;
  private localNumberFormatter: NumberFormattingPipe;
  private localElRef: ElementRef;
  private localCommunicator: CommunicatorService;

  constructor(calcService: CalcService, numberFormatter: NumberFormattingPipe, elRef: ElementRef, communicator: CommunicatorService) {
    super(calcService,numberFormatter,elRef,communicator)

    this.localCalcService = calcService;
    this.localNumberFormatter = numberFormatter;
    this.localElRef = elRef;
    this.localCommunicator = communicator;
  }

  ngOnInit() {
    if(this.outputMin && (this.outputMin.toString().indexOf('tlOutput') != -1 || this.outputMin.toString().indexOf('tlInput') != -1)){
      this.outputMin = +this.localCalcService.getValue(this.outputMin.toString(), true);
    }
    if(this.outputMax && (this.outputMax.toString().indexOf('tlOutput') != -1 || this.outputMax.toString().indexOf('tlInput') != -1)){
      this.outputMax = +this.localCalcService.getValue(this.outputMax.toString(), true);
    }
    if(this.step && (this.step.toString().indexOf('tlOutput') != -1 || this.step.toString().indexOf('tlInput') != -1)){
      this.step = +this.localCalcService.getValue(this.step.toString(), true);
    }
    super.ngOnInit();

    if(this.outputRangeRef != ''){
      this.outputRangeValue = this.localCalcService.getValueForYear(this.outputRangeRef,this.yearRef);
      this.hasDifferentOutput = true;
    }
  }

  /**
   *
   * Function to format the updated value and save to model
   *
   * @param {value} value Updated value
   *
   */
  onStepperButtonClick(value) {

    // if (!this.isFocused) {
    //   this.inputValueRef.nativeElement.focus();
    //   this.onFocus();
    //   return;
    // }

    if (this.format === '0%') {
      value = '' + Math.round(value);
    } else if (this.format === '0.0%') {
      value = '' + parseFloat(value).toFixed(1);
    } else if (this.format === '0.00%') {
      value = '' + parseFloat(value).toFixed(2);
    }

    let prevalue = this.value;
    if (Number(value) != NaN && this.value !== value) {
        this.value = value;
        const modelValue = value == "0" ? 0 : this.localNumberFormatter.parse(this.localNumberFormatter.transform(value, this.inputFormat, this.inputScaler));
        this.localCalcService.setValueForYear(this.inRef, modelValue, this.yearRef);
        // this.formattedVal = this.localNumberFormatter.transform(this.value, this.format, this.scaler) + '';
    }

    // if (this.value !== value) {
    //   this.value = value;
    //   let modelValue = this.numberFormatter.parse(this.numberFormatter.transform(value, this.inputFormat, this.inputScaler));
    //   modelValue = modelValue.toString().split('$').join('');
    //   this.calcService.setValueForYear(this.inRef, modelValue, this.yearRef);
    // }
  }

  /**
   * Function to format and display the updated value
   *
   */
  onModelChange() {
    super.onModelChange();

    if(this.hasDifferentOutput){
      this.outputRangeValue = this.localCalcService.getValueForYear(this.outputRangeRef,this.yearRef);
    }
  }

  /**
   * Function to check if stepping down doesn't goes out of bounds
   *
   */
  validator(amount) {
    const outVal = this.localCalcService.getValueForYear(this.outRef, this.yearRef, true) as any;
    amount = this.localNumberFormatter.parse(this.localNumberFormatter.transform(amount, this.inputFormat, this.inputScaler));

    // check if stepping down goes out of bounds
    if (this.outputMin !== undefined && amount < 0) {
      if ((outVal + amount)*this.scaler < Number(this.outputMin)) {
        return false;
      }
    } else if (this.outputMax !== undefined && amount > 0) {
      if ((outVal + amount)*this.scaler > Number(this.outputMax)) {
        return false;
      }
    }
    return true;
  }

  saveDataToModel(enteredValue) {
    super.saveDataToModel(enteredValue);
  }

}
