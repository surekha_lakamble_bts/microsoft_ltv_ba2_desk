import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CalcService, IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CalcServiceStub } from '../../test/calc-service.stub';
import { ChartComponentComponent } from './chart-component.component';

describe('ChartComponentComponent', () => {
  let component: ChartComponentComponent;
  let fixture: ComponentFixture<ChartComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IsomerCoreModule, CarouselModule.forRoot()],
      declarations: [ChartComponentComponent],
      providers: [{ provide: CalcService, useClass: CalcServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponentComponent);
    component = fixture.componentInstance;
    component.chartData = {
      chartOptions: {
        chart: {
          height: 200,
          // width: 225,
          margin: [20, 0, 40, 0],
          spacing: [0, 0, 0, 0]
        },
        title: {
          text: 'Forecasted Bookings ($m)',
          margin: 10
        },
        plotOptions: {
          column: {
            colorByPoint: true,
            groupPadding: 0.1,
            pointPadding: 0,
            dataLabels: {
              enabled: true,
              crop: false,
              overflow: 'none',
              format: '{y:$0,0a}',
              padding: 2
            }
          }
        },
        tooltip: {
          enabled: true,
          pointFormat: '{categories}<br />{series.name}: {point.y:$0,0a}'
        }
      },
      rangeRef: [['tlOutputRainfallP1', 'tlOutputRainfallP2', 'tlOutputRainfallP3']],
      seriesOptions: [{
        showInLegend: false,
        type: 'column',
        name: 'Revenue'
      }],
      xAxis: {
        categories: ['CRM', 'HCM', 'Futuria']
      },
      yAxis: {
        visible: false
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
