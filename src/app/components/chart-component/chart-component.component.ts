import { Component, OnInit, Input } from '@angular/core';
import { ChartData } from '../../interfaces';

/**
 * Chart component renders the chart based on the provided input
 * Chart is rendered using ism-chart component of ngx-isomer-core & highcharts
 *
 */
@Component({
  selector: 'ism-chart-component',
  templateUrl: './chart-component.component.html',
  styleUrls: ['./chart-component.component.styl']
})
export class ChartComponentComponent {

  /**
   * Input binding for chartData which will render the chart
   *
   */
  @Input() chartData: ChartData;

}
