import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { CarouselModule } from 'ngx-bootstrap/carousel';

import { AccordianComponent } from './accordian/accordian.component';
import { ChartComponentComponent } from './chart-component/chart-component.component';
import { MetricTableComponent } from './metric-table/metric-table.component';
import { MetricChartComponent } from './metric-chart/metric-chart.component';
import { SelectionComponent } from './selection/selection.component';
import { FormsModule } from '@angular/forms';
import { ReportTableComponent } from './report-table/report-table.component';
import { WobblerComponent } from './wobbler/wobbler.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ViewmodeComponent } from './viewmode/viewmode.component';
import { OnlynumberDirective } from './only-number.directive';
import { CalcStepperComponent } from './calcstepper/calcstepper.component'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CarouselModule.forRoot(),
    IsomerCoreModule,
    FormsModule
  ],
  declarations: [ChartComponentComponent, MetricTableComponent, AccordianComponent, MetricChartComponent, ReportTableComponent, SelectionComponent, WobblerComponent, FooterComponent, HeaderComponent, ViewmodeComponent, OnlynumberDirective, CalcStepperComponent],
  exports: [ChartComponentComponent, MetricTableComponent, AccordianComponent, MetricChartComponent, ReportTableComponent, SelectionComponent, WobblerComponent, FooterComponent, HeaderComponent, ViewmodeComponent, OnlynumberDirective, CalcStepperComponent]
})
export class ComponentsModule { }
