import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CalcService, IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CalcServiceStub } from '../../test/calc-service.stub';
import { MetricTableComponent } from '../metric-table/metric-table.component';
import { FooterComponent } from './footer.component';
import { ChartComponentComponent } from '../chart-component/chart-component.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let el: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, IsomerCoreModule, CarouselModule.forRoot()],
      declarations: [FooterComponent, MetricTableComponent, ChartComponentComponent],
      providers: [{ provide: CalcService, useClass: CalcServiceStub }]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    component.footerData = {
      mainContainer: { style: { 'padding': '7px 8px 5px 30px' } },
      logo: 'assets/images/btslogofooter.svg',
      navMetricTableData: {
        withCarousel: false,
        tableData: [{
          slideData: [{
            rowData: [{
              colspanClass: 'col-2',
              ref: 'Bookings'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: 'tlOutputRainfallP1',
              showPositiveBalloon: true,
              format: '0.0a'
            }, {
              colspanClass: 'col-2',
              ref: 'Bookings CAGR'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: 'tlOutputTempP6',
              showNegativeBalloon: true,
              format: '0%',
              scaler: 100
            }, {
              colspanClass: 'col',
              ref: 'Revenues'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: '104',
              showPositiveBalloon: true
            }, {
              colspanClass: 'col',
              ref: 'GM',
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: '10.13b',
              showPositiveBalloon: true
            }, {
              colspanClass: 'col',
              ref: 'OM'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: '86',
              showPositiveBalloon: true
            }]
          }, {
            rowData: [{
              colspanClass: 'col-2',
              ref: 'GW Hrs Expense'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: '1,270m'
            }, {
              colspanClass: 'col-2',
              ref: 'Emp NPS'
            }, {
              colspanClass: 'col',
              fontWeight: 500,
              ref: '68%',
            }, {
              colspanClass: 'col',
              ref: 'Cust NPS'
            }, {
              colspanClass: 'col-5',
              fontWeight: 500,
              ref: '65%',
            }]
          }]
        }]
      }
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display matric table in footer', () => {
    expect(el.querySelector('.dashboard-metric-table-wrapper')).toBeTruthy();
    expect(el.querySelector('.copyright-text')).toBeFalsy();

  });

  it('should display copy text in footer', () => {
    component.footerData = {
      mainContainer: { style: { 'padding': '7px 8px 5px 30px', color: '$white' } },
      desc: '© 2018 BTS Digital. All rights reserved',
      logo: 'assets/images/btslogofooter.svg'
    };
    fixture.detectChanges();
    expect(el.querySelector('.copyright-text')).toBeTruthy();
    expect(el.querySelector('.dashboard-metric-table-wrapper')).toBeFalsy();
  });

});
