import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ism-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.styl']
})
export class FooterComponent implements OnInit {

  @Input() footerData: any;

  constructor() { }

  ngOnInit() {
  }

}
