import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CalcService, IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { CalcServiceStub } from '../../test/calc-service.stub';
import { ChartComponentComponent } from '../chart-component/chart-component.component';
import { MetricChartComponent } from '../metric-chart/metric-chart.component';
import { HeaderComponent } from './header.component';

fdescribe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let el: any;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, CarouselModule.forRoot(), RouterTestingModule, IsomerCoreModule],
      declarations: [HeaderComponent, ChartComponentComponent, MetricChartComponent],
      providers: [{ provide: CalcService, useClass: CalcServiceStub }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    component.headerData = {
      headerLogo: 'assets/images/btslogo.svg',
      homeIcon: 'icon-home',
      type: 'productheader',
      // decisionIcon: 'icon-compass',
      headerHeight: { height: '200px' },
      chartmetrics: [
        {
          rangeRef: [['tlOutputRainfallP1']],
          yAxis: {
            labels: {
              enabled: false
            },
            min: 0,
            max: 1000000000
          },
          seriesOptions: [{
            type: 'solidgauge',
            name: 'TeamName1'
          }],
          chartOptions: {
            title: {
              text: 'Gross Margin',
              floating: true,
              align: 'center',
              x: 0,
              y: 80
            },
            chart: {
              height: 105,
              // width: 200,
              margin: [15, 0, 0, 0],
              spacing: [10, 0, 0, 0],
              marginTop: -18,
              className: 'limited-dashboard-metric-solid-gauge'
            },
            plotOptions: {
              solidgauge: {
                className: 'metric-solid-guage',
                innerRadius: '80%',
                outerRadius: '100%',
                rounded: false,
                dataLabels: {
                  className: 'solid-gauge-label',
                  format: '{y:0%:1000000000}',
                  y: -25
                }
              }
            },
            pane: {
              startAngle: -90,
              endAngle: 90,
              background: [{
                innerRadius: '70%',
                outerRadius: '110%',
                borderWidth: 0
              }],
              shape: 'arc',
              size: '100%'
            },
            tooltip: {
              enabled: false
            }
          }
        }, {
          key: '$11.9M',
          title: 'Revenue'
        }, {
          rangeRef: [['tlOutputRainfallP3']],
          yAxis: {
            labels: {
              enabled: false
            },
            min: 0,
            max: 1000000000
          },
          seriesOptions: [{
            type: 'solidgauge',
            name: 'TeamName1'
          }],
          chartOptions: {
            title: {
              text: 'Operating Margin',
              floating: true,
              align: 'center',
              x: 0,
              y: 80
            },
            chart: {
              height: 105,
              // width: 200,
              margin: [15, 0, 0, 0],
              spacing: [10, 0, 0, 0],
              marginTop: -18,
              className: 'limited-dashboard-metric-solid-gauge'
            },
            plotOptions: {
              solidgauge: {
                className: 'metric-solid-guage',
                innerRadius: '80%',
                outerRadius: '100%',
                rounded: false,
                dataLabels: {
                  className: 'solid-gauge-label',
                  format: '{y:0%:1000000000}',
                  y: -25
                }
              }
            },
            pane: {
              startAngle: -90,
              endAngle: 90,
              background: [{
                innerRadius: '70%',
                outerRadius: '110%',
                borderWidth: 0
              }],
              shape: 'arc',
              size: '100%'
            },
            tooltip: {
              enabled: false
            }
          }
        }, {
          rangeRef: [['tlOutputRainfallP3']],
          yAxis: {
            labels: {
              enabled: false
            },
            min: 0,
            max: 1000000000
          },
          seriesOptions: [{
            type: 'solidgauge',
            name: 'TeamName1'
          }],
          chartOptions: {
            title: {
              text: 'Operating Margin',
              floating: true,
              align: 'center',
              x: 0,
              y: 80
            },
            chart: {
              height: 105,
              // width: 200,
              margin: [15, 0, 0, 0],
              spacing: [10, 0, 0, 0],
              marginTop: -18,
              className: 'limited-dashboard-metric-solid-gauge'
            },
            plotOptions: {
              solidgauge: {
                className: 'metric-solid-guage',
                innerRadius: '80%',
                outerRadius: '100%',
                rounded: false,
                dataLabels: {
                  className: 'solid-gauge-label',
                  format: '{y:0%:1000000000}',
                  y: -25
                }
              }
            },
            pane: {
              startAngle: -90,
              endAngle: 90,
              background: [{
                innerRadius: '70%',
                outerRadius: '110%',
                borderWidth: 0
              }],
              shape: 'arc',
              size: '100%'
            },
            tooltip: {
              enabled: false
            }
          }
        }],
    };
    fixture.detectChanges();
  });

  it('should create', fakeAsync(() => {
    tick(1000);
    expect(component).toBeTruthy();
  }));

  it('should display on product page', () => {
    expect(component.headerData.type).toBe('productheader');
  });

  it('should display on Sales Marketing Page', () => {
    component.headerData = {
      headerLogo: 'assets/images/btslogo.svg',
      homeIcon: 'icon-home',
      type: 'limiteddashboardheader',
      decisionIcon: 'icon-compass',
      headerHeight: { height: '160px' },
      chartmetrics: [
        {
          rangeRef: [['tlOutputRainfallP1']],
          yAxis: {
            labels: {
              enabled: false
            },
            min: 0,
            max: 1000000000
          },
          seriesOptions: [{
            type: 'solidgauge',
            name: 'TeamName1'
          }],
          chartOptions: {
            title: {
              text: 'Gross Margin',
              floating: true,
              align: 'center',
              x: 0,
              y: 80
            },
            chart: {
              height: 105,
              // width: 100,
              margin: [15, 0, 0, 0],
              spacing: [10, 0, 0, 0],
              marginTop: -18,
              className: 'limited-dashboard-metric-solid-gauge'
            },
            plotOptions: {
              solidgauge: {
                className: 'metric-solid-guage',
                innerRadius: '80%',
                outerRadius: '100%',
                rounded: false,
                dataLabels: {
                  className: 'solid-gauge-label',
                  format: '{y:0%:1000000000}',
                  y: -25
                }
              }
            },
            pane: {
              startAngle: -90,
              endAngle: 90,
              background: [{
                innerRadius: '70%',
                outerRadius: '110%',
                borderWidth: 0
              }],
              shape: 'arc',
              size: '100%'
            },
            tooltip: {
              enabled: false
            }
          }
        }, {
          key: '$11.9M',
          title: 'Revenue'
        }, {
          rangeRef: [['tlOutputRainfallP3']],
          yAxis: {
            labels: {
              enabled: false
            },
            min: 0,
            max: 1000000000
          },
          seriesOptions: [{
            type: 'solidgauge',
            name: 'TeamName1'
          }],
          chartOptions: {
            title: {
              text: 'Operating Margin',
              floating: true,
              align: 'center',
              x: 0,
              y: 80
            },
            chart: {
              height: 105,
              // width: 100,
              margin: [15, 0, 0, 0],
              spacing: [10, 0, 0, 0],
              marginTop: -18,
              className: 'limited-dashboard-metric-solid-gauge'
            },
            plotOptions: {
              solidgauge: {
                className: 'metric-solid-guage',
                innerRadius: '80%',
                outerRadius: '100%',
                rounded: false,
                dataLabels: {
                  className: 'solid-gauge-label',
                  format: '{y:0%:1000000000}',
                  y: -25
                }
              }
            },
            pane: {
              startAngle: -90,
              endAngle: 90,
              background: [{
                innerRadius: '70%',
                outerRadius: '110%',
                borderWidth: 0
              }],
              shape: 'arc',
              size: '100%'
            },
            tooltip: {
              enabled: false
            }
          }
        }],
    };
    fixture.detectChanges();
    expect(component.headerData.type).toBe('limiteddashboardheader');
  });

});
