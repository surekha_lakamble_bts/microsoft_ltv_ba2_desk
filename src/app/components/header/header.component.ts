import { Component, OnInit, Input, HostListener, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ism-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  @Input() headerData: any;
  showMenu: boolean;
  constructor() { }

  ngOnInit() {
  }

  // isShow() {
  //   this.showMenu = !this.showMenu;
  // }

  // clickLink() {

  // }

  // @HostListener('click', ['$event']) clickedOutside($event) {
  //   console.log('lllllllllll');
  //   const menu = document.querySelector('.icon-programs-fill');
  //   if (menu.className !== $event.target.className) {
  //     this.showMenu = false;
  //   }
  // }
}
