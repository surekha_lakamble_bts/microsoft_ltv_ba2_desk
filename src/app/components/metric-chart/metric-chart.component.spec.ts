import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricChartComponent } from './metric-chart.component';
import { ChartComponentComponent } from '../chart-component/chart-component.component';
import { IsomerCoreModule, CalcService } from '@btsdigital/ngx-isomer-core';
import { CalcServiceStub } from '../../test/calc-service.stub';

describe('MetricChartComponent', () => {
  let component: MetricChartComponent;
  let fixture: ComponentFixture<MetricChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IsomerCoreModule],
      declarations: [MetricChartComponent, ChartComponentComponent],
      providers: [{ provide: CalcService, useClass: CalcServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricChartComponent);
    component = fixture.componentInstance;
    component.chartData = [
      {
        rangeRef: [['tlOutputRainfallP1']],
        yAxis: {
          labels: {
            enabled: false
          },
          min: 0,
          max: 1000000000
        },
        seriesOptions: [{
          type: 'solidgauge',
          name: 'TeamName1'
        }],
        chartOptions: {
          title: {
            text: 'Gross Margin',
            floating: true,
            align: 'center',
            x: 0,
            y: 80
          },
          chart: {
            height: 105,
            // width: 200,
            margin: [15, 0, 0, 0],
            spacing: [10, 0, 0, 0],
            marginTop: -18,
            className: 'limited-dashboard-metric-solid-gauge'
          },
          plotOptions: {
            solidgauge: {
              className: 'metric-solid-guage',
              innerRadius: '80%',
              outerRadius: '100%',
              rounded: false,
              dataLabels: {
                className: 'solid-gauge-label',
                format: '{y:0%:1000000000}',
                y: -25
              }
            }
          },
          pane: {
            startAngle: -90,
            endAngle: 90,
            background: [{
              innerRadius: '70%',
              outerRadius: '110%',
              borderWidth: 0
            }],
            shape: 'arc',
            size: '100%'
          },
          tooltip: {
            enabled: false
          }
        }
      }, {
        rangeRef: [['tlOutputRainfallP3']],
        yAxis: {
          labels: {
            enabled: false
          },
          min: 0,
          max: 1000000000
        },
        seriesOptions: [{
          type: 'solidgauge',
          name: 'TeamName1'
        }],
        chartOptions: {
          title: {
            text: 'Operating Margin',
            floating: true,
            align: 'center',
            x: 0,
            y: 80
          },
          chart: {
            height: 105,
            // width: 200,
            margin: [15, 0, 0, 0],
            spacing: [10, 0, 0, 0],
            marginTop: -18,
            className: 'limited-dashboard-metric-solid-gauge'
          },
          plotOptions: {
            solidgauge: {
              className: 'metric-solid-guage',
              innerRadius: '80%',
              outerRadius: '100%',
              rounded: false,
              dataLabels: {
                className: 'solid-gauge-label',
                format: '{y:0%:1000000000}',
                y: -25
              }
            }
          },
          pane: {
            startAngle: -90,
            endAngle: 90,
            background: [{
              innerRadius: '70%',
              outerRadius: '110%',
              borderWidth: 0
            }],
            shape: 'arc',
            size: '100%'
          },
          tooltip: {
            enabled: false
          }
        }
      }, {
        rangeRef: [['tlOutputRainfallP3']],
        yAxis: {
          labels: {
            enabled: false
          },
          min: 0,
          max: 1000000000
        },
        seriesOptions: [{
          type: 'solidgauge',
          name: 'TeamName1'
        }],
        chartOptions: {
          title: {
            text: 'Operating Margin',
            floating: true,
            align: 'center',
            x: 0,
            y: 80
          },
          chart: {
            height: 105,
            // width: 200,
            margin: [15, 0, 0, 0],
            spacing: [10, 0, 0, 0],
            marginTop: -18,
            className: 'limited-dashboard-metric-solid-gauge'
          },
          plotOptions: {
            solidgauge: {
              className: 'metric-solid-guage',
              innerRadius: '80%',
              outerRadius: '100%',
              rounded: false,
              dataLabels: {
                className: 'solid-gauge-label',
                format: '{y:0%:1000000000}',
                y: -25
              }
            }
          },
          pane: {
            startAngle: -90,
            endAngle: 90,
            background: [{
              innerRadius: '70%',
              outerRadius: '110%',
              borderWidth: 0
            }],
            shape: 'arc',
            size: '100%'
          },
          tooltip: {
            enabled: false
          }
        }
      }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
