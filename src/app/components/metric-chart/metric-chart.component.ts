import { Component, OnInit, Input, ViewEncapsulation, DoCheck } from '@angular/core';
import { ChartData } from '../../interfaces';
import { CalcService } from '@btsdigital/ngx-isomer-core';
import { element } from 'protractor';
import * as Rx from 'rxjs/Rx';
/**
 * MetricChart component renders the chart based on the provided input
 * Chart is rendered using ism-metric-chart component of ngx-isomer-core & highcharts
 *
 */
@Component({
  selector: 'ism-metric-chart',
  templateUrl: './metric-chart.component.html',
  styleUrls: ['./metric-chart.component.styl'],
  encapsulation: ViewEncapsulation.None
})
export class MetricChartComponent implements OnInit {
  /**
     * Input binding for sgChartData which will render the chart
     *
     */
  @Input() chartData: any[];

  @Input() showBalloon: boolean;
  @Input() format: string;
  value: number;
  newValue: number;
  flag: string;
  count = 0;
  constructor(private calcservice: CalcService) {

  }

  ngOnInit() {

    this.chartData.filter(i => i.rangeRef).map(m => m.value = this.calcservice.getValue(m.rangeRef[0][0]) ? Number(this.calcservice.getValue(m.rangeRef[0][0]).replace(/\D/g, '')) : null);

    this.calcservice.getObservable().debounceTime(1000).subscribe((refname) => {

      this.chartData.filter((e: any) => e.showBalloon).forEach((ele) => {

        if (refname === ele.rangeRef[0][0]) {
          const calcvalue = this.calcservice.getValue(refname) ? Number(this.calcservice.getValue(refname).replace(/\D/g, '')) : null;
          if (calcvalue > ele.value) {
            ele.flag = 'positive';
            ele.newValue = calcvalue - ele.value;

          } else {
            ele.flag = 'negative';
            ele.newValue = ele.value - calcvalue;
          }

          setTimeout(() => {
            ele.flag = null;
          }, 2000);

          ele.value = calcvalue;
        }

      });
      this.count++;

    });

  }


}
