import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { IsomerCoreModule, TextService, CalcService } from '@btsdigital/ngx-isomer-core';
import { MetricTableComponent } from './metric-table.component';
import { CalcServiceStub } from '../../test/calc-service.stub';
import { TextServiceStub } from '../../test/text-service.stub';

describe('MetricTableComponent', () => {
  let component: MetricTableComponent;
  let fixture: ComponentFixture<MetricTableComponent>;
  let calcService: CalcServiceStub,
    textService: TextServiceStub;

  const dataRefs = ['tlOutputRainfallP1', 'tlOutputRainfallP2'];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IsomerCoreModule, CarouselModule.forRoot()],
      declarations: [MetricTableComponent],
      providers: [
        { provide: CalcService, useClass: CalcServiceStub },
        { provide: TextService, useClass: TextServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    calcService = TestBed.get(CalcService);
    textService = TestBed.get(TextService);
    fixture = TestBed.createComponent(MetricTableComponent);
    component = fixture.componentInstance;
    calcService.apiReady = true;
    textService.isReady = true;
    textService.textContent.GEN = {
      'SalesForecast': 'Sales Forecast',
      'HCM': 'HCM'
    };
    dataRefs.forEach((ref) => {
      const random = Math.random() * 1000000;
      calcService.setValue(ref, random);
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create create table without carousel', () => {
    component.metricTableData = {
      withCarousel: false,
      tableData: [{
        slideData: [{
          rowData: [{
            colspanClass: 'col-2',
            ref: 'Bookings'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: 'tlOutputRainfallP2',
            showPositiveBalloon: true,
            format: '0,0a'
          }, {
            colspanClass: 'col-2',
            ref: 'Bookings CAGR'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '9.8%',
            showNegativeBalloon: true
          }, {
            colspanClass: 'col-1',
            ref: 'Revenues'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '104',
            showPositiveBalloon: true
          }, {
            colspanClass: 'col-1',
            ref: 'GM',
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '10.13b',
            showPositiveBalloon: true
          }, {
            colspanClass: 'col-1',
            ref: 'OM'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '86',
            showPositiveBalloon: true
          }]
        }, {
          rowData: [{
            colspanClass: 'col-2',
            ref: 'GW Hrs Expense'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '1,270m'
          }, {
            colspanClass: 'col-2',
            ref: 'Emp NPS'
          }, {
            colspanClass: 'col-1',
            fontWeight: 500,
            ref: '68%',
          }, {
            colspanClass: 'col-1',
            ref: 'Cust NPS'
          }, {
            colspanClass: 'col-5',
            fontWeight: 500,
            ref: '65%',
          }]
        }]
      }]
    };
    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.metricTableData.withCarousel).toBeFalsy();
  });

  it('should create table without carousel and full width', () => {
    component.metricTableData = {
      withCarousel: false,
      wideScreen: true,
      tableData: [{
        slideData: [{
          rowData: [{
            colspanClass: 'col-3',
            ref: 'Bookings'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: 'tlOutputRainfallP1',
            showPositiveBalloon: true,
            format: '0.0a'
          }, {
            colspanClass: 'col-3',
            ref: 'Bookings CAGR'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: 'tlOutputTempP6',
            showNegativeBalloon: true,
            format: '0%',
            scaler: 100
          }, {
            colspanClass: 'col-3',
            ref: 'Revenues'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '104',
            showPositiveBalloon: true
          }, {
            colspanClass: 'col-3',
            ref: 'GM',
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '10.13b',
            showPositiveBalloon: true
          }, {
            colspanClass: 'col-3',
            ref: 'OM'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '86',
            showPositiveBalloon: true
          }, {
            colspanClass: 'col-3',
            ref: 'GW Hrs Expense'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '1,270m'
          }, {
            colspanClass: 'col-3',
            ref: 'Emp NPS'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '68%',
          }, {
            colspanClass: 'col-3',
            ref: 'Cust NPS'
          }, {
            colspanClass: 'col-3',
            fontWeight: 500,
            ref: '65%',
          }]
        }]
      }]
    };

    component.ngOnInit();
    fixture.detectChanges();
    expect(component.metricTableData.withCarousel).toBeFalsy();
    expect(component.metricTableData.wideScreen).toBeTruthy();
  });

  it('should create create table with carousel', () => {
    component.metricTableData = {
      tableData: [{
        slideData: [{
          rowData: [{
            colspanClass: 'col-4',
            fontWeight: 100,
            ref: 'SalesForecast',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false
          }, {
            fontWeight: 100,
            ref: 'CRM',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 100,
            ref: 'HCM',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 100,
            ref: 'Futuria',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }]
        }, {
          rowData: [{
            colspanClass: 'col-4',
            fontWeight: 500,
            ref: 'Average Bill Rate',
            isHighlighted: true,
            showPositiveBalloon: false,
            showNegativeBalloon: false
          }, {
            fontWeight: 500,
            ref: '2',
            isHighlighted: false,
            showPositiveBalloon: true,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 500,
            ref: '1',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: true,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 500,
            ref: '1',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: true,
            textAlignClass: 'text-center'
          }]
        }]
      }, {
        slideData: [{
          rowData: [{
            colspanClass: 'col-4',
            fontWeight: 100,
            ref: 'Project Staffing',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false
          }, {
            fontWeight: 100,
            ref: 'Director',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 100,
            ref: 'PM',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 100,
            ref: 'Technical',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }]
        }, {
          rowData: [{
            colspanClass: 'col-4',
            fontWeight: 500,
            ref: 'On Site',
            isHighlighted: true,
            showPositiveBalloon: false,
            showNegativeBalloon: false
          }, {
            fontWeight: 500,
            ref: 'tlOutputRainfallP1',
            isHighlighted: false,
            showPositiveBalloon: true,
            showNegativeBalloon: false,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 500,
            ref: '110,000',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: true,
            textAlignClass: 'text-center'
          }, {
            fontWeight: 500,
            ref: '175,000',
            isHighlighted: false,
            showPositiveBalloon: false,
            showNegativeBalloon: true,
            textAlignClass: 'text-center'
          }]
        }]
      }]
    };
    fixture.detectChanges();
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.metricTableData.withCarousel).toBeTruthy();
  });

});
