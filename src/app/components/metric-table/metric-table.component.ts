import { Component, OnInit, Input } from '@angular/core';
import { CalcService, TextService, NumberFormattingPipe } from '@btsdigital/ngx-isomer-core';
import * as _ from 'lodash';
import { MetricTableData } from '../../interfaces';

/**
 * Metric Table Component for displaying metrics with/without carousel
 *
 */
@Component({
  selector: 'ism-metric-table',
  templateUrl: './metric-table.component.html',
  styleUrls: ['./metric-table.component.styl']
})
export class MetricTableComponent implements OnInit {
  /**
   * Input binding for metricTableData which will display the metrics based on the configuration
   */
  @Input() metricTableData: MetricTableData;

  /**
   * Constructor for Metric Table Component
   *
   * @param {CalcService} calcService CalcService Instance
   *
   * @param {TextService} textService TextService Instance
   *
   * @param {NumberFormattingPipe} numFormat NumberFormattingPipe instance
   */
  constructor(private calcService: CalcService, private textService: TextService, private numFormat: NumberFormattingPipe) { }

  /**
   * OnInit function for component
   *
   * We fetch the values for the ref from the calcService/textEngine
   */
  ngOnInit() {

    if (this.metricTableData) {

      if (this.metricTableData.withCarousel === undefined) {
        this.metricTableData.withCarousel = true;
      }
      _.each(this.metricTableData.tableData, (td) => {
        _.each(td.slideData, (sd) => {
          _.each(sd.rowData, (rd) => {
            rd.ref = (/tl(In|Out)put.+/i.test(rd.ref)) ? // check if value can be fetched from calc model
              ((this.calcService.getValueForYear(rd.ref, rd.yearRef)) ?
                this.calcService.getValueForYear(rd.ref, rd.yearRef) :
                this.calcService.getValue(rd.ref)) : // get value for year / get value
              this.textService.getTextForYear(rd.ref, rd.yearRef) ?
                this.textService.getTextForYear(rd.ref, rd.yearRef) :
                this.textService.getText(rd.ref) ? // get text for year / get text
                  this.textService.getText(rd.ref) : rd.ref; // fallback to the label passed
            if (rd.format) {
              rd.ref = (rd.scaler ? this.numFormat.transform(rd.ref, rd.format, rd.scaler) :
                this.numFormat.transform(rd.ref, rd.format)) as any;
            }
          });
          if (this.metricTableData.wideScreen) {
            sd.rowData = _.chunk(sd.rowData, 4) as any;
          }
        });
      });
    }
  }

}
