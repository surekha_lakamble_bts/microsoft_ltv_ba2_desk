import { OnlynumberDirective } from './only-number.directive';
// import { SafeHtmlPipe } from './safe-html.pipe';
import { TestBed, inject } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { ElementRef } from '@angular/core';

// describe('OnlyNumberDirective', () => {
//   it('should create an instance', () => {
//     const directive = new OnlynumberDirective(1212);
//     expect(directive).toBeTruthy();
//   });
// });
// )
describe('SanitiseHtmlPipe', () => {
  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [
          BrowserModule
        ]
      });
  });

  it('create an instance', inject([OnlynumberDirective], (value: ElementRef) => {
    const pipe = new OnlynumberDirective(value);
    expect(pipe).toBeTruthy();
  }));

});
