import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IsomerCoreModule, TextService, CalcService } from '@btsdigital/ngx-isomer-core';
import { CalcServiceStub } from '../../test/calc-service.stub';
import { TextServiceStub } from '../../test/text-service.stub';

import { ReportTableComponent } from './report-table.component';

describe('ReportTableComponent', () => {
  let component: ReportTableComponent;
  let fixture: ComponentFixture<ReportTableComponent>;
  let calcService: CalcServiceStub,
    textService: TextServiceStub;

  const dataRefs = ['tlOutputRainfallP1', 'tlOutputRainfallP2'];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [IsomerCoreModule],
      declarations: [ReportTableComponent],
      providers: [
        { provide: CalcService, useClass: CalcServiceStub },
        { provide: TextService, useClass: TextServiceStub }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    calcService = TestBed.get(CalcService);
    textService = TestBed.get(TextService);
    fixture = TestBed.createComponent(ReportTableComponent);
    component = fixture.componentInstance;
    calcService.apiReady = true;
    textService.isReady = true;
    textService.textContent.GEN = {
      'SalesForecast': 'Sales Forecast',
      'HCM': 'HCM'
    };
    dataRefs.forEach((ref) => {
      const random = Math.random() * 1000000;
      calcService.setValue(ref, random);
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the table', () => {
    component.reportTableData = [{
      keyRef: 'Total Revenue',
      valueRef: 'tlOutputRainfallP1',
      keyFontWeight: 'bold',
      valueFontWeight: 'bold',
      format: '$0,0a'
    }, {
      keyRef: 'Revenue CAGR%',
      valueRef: 'tlOutputTempP1',
      keyFontStyle: 'italic',
      valueFontStyle: 'italic',
      keyFontWeight: 'bold',
      valueFontWeight: 'bold',
      format: '0%',
      scaler: 100
    }, {
      keyRef: 'Cost of Goods Sold',
      valueRef: '$4.3 M',
      keyFontWeight: 500,
      highlightRow: true
    }, {
      keyRef: 'Gross Margin',
      valueRef: '$7.6 M',
      keyFontWeight: 'bold',
      valueFontWeight: 'bold',
      highlightRow: true
    }, {
      keyRef: 'Gross Margin %',
      valueRef: '64.0%',
      keyFontWeight: 'bold',
      valueFontWeight: 'bold',
      highlightRow: true
    }, {
      keyRef: 'Research & Development',
      valueRef: '$2.3 M',
      keyFontWeight: 500
    }, {
      keyRef: 'Go to Market',
      valueRef: '$3.9 M',
      keyFontWeight: 500
    }, {
      keyRef: 'General & Administrative',
      valueRef: '$0.6 M',
      keyFontWeight: 500
    }, {
      isLine: true
    }, {
      keyRef: 'Total Operating Expenses',
      valueRef: '$6.8 M',
      keyFontWeight: 500,
      valueFontWeight: 500
    }, {
      keyRef: 'Operating Profit',
      valueRef: '$0.9 M',
      keyFontWeight: 500,
      valueFontWeight: 500
    }, {
      keyRef: 'Operating Margin %',
      valueRef: '7.2%',
      keyFontStyle: 'italic',
      valueFontStyle: 'italic',
      keyFontWeight: 'bold',
      valueFontWeight: 'bold'
    }];

    component.ngOnInit();
    fixture.detectChanges();
  });
});
