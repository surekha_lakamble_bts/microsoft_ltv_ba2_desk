import { Component, OnInit, Input } from '@angular/core';
import { CalcService, TextService, NumberFormattingPipe } from '@btsdigital/ngx-isomer-core';
import { ReportTable } from '../../interfaces';
import * as _ from 'lodash';

/**
 * Report Table Component for displaying income statements, revenues, etc.
 *
 */
@Component({
  selector: 'ism-report-table',
  templateUrl: './report-table.component.html',
  styleUrls: ['./report-table.component.styl']
})

export class ReportTableComponent implements OnInit {

  /**
   * Input binding for reportTableData which will display the table based on the configuration
   */
  @Input() reportTableData: ReportTable[];

  /**
   * Constructor for Report Table Component
   *
   * @param {CalcService} calcService CalcService Instance
   *
   * @param {TextService} textService TextService Instance
   *
   * @param {NumberFormattingPipe} numFormat NumberFormattingPipe instance
   */
  constructor(private calcService: CalcService, private textService: TextService, private numFormat: NumberFormattingPipe) { }

  /**
   * OnInit function for component
   *
   * We fetch the values for the keyRef and valueRef from the calcService/textEngine
   */
  ngOnInit() {

    if (this.reportTableData) {
      _.each(this.reportTableData, (rd) => {
        if (rd.keyRef) {
          rd.keyRef = (/tl(In|Out)put.+/i.test(rd.keyRef)) ? // check if value can be fetched from calc model
            ((this.calcService.getValueForYear(rd.keyRef, rd.keyYearRef)) ?
              this.calcService.getValueForYear(rd.keyRef, rd.keyYearRef) :
              this.calcService.getValue(rd.keyRef)) : // get value for year / get value
            this.textService.getTextForYear(rd.keyRef, rd.keyYearRef) ?
              this.textService.getTextForYear(rd.keyRef, rd.keyYearRef) :
              this.textService.getText(rd.keyRef) ? // get text for year / get text
                this.textService.getText(rd.keyRef) : rd.keyRef; // fallback to the label passed
        }

        if (rd.valueRef) {
          rd.valueRef = (/tl(In|Out)put.+/i.test(rd.valueRef)) ? // check if value can be fetched from calc model
            ((this.calcService.getValueForYear(rd.valueRef, rd.valueYearRef)) ?
              this.calcService.getValueForYear(rd.valueRef, rd.valueYearRef) :
              this.calcService.getValue(rd.valueRef)) : // get value for year / get value
            this.textService.getTextForYear(rd.valueRef, rd.valueYearRef) ?
              this.textService.getTextForYear(rd.valueRef, rd.valueYearRef) :
              this.textService.getText(rd.valueRef) ? // get text for year / get text
                this.textService.getText(rd.valueRef) : rd.valueRef; // fallback to the label passed
        }

        if (rd.valueRef && rd.format) {
          rd.valueRef = (rd.scaler ? this.numFormat.transform(rd.valueRef, rd.format, rd.scaler) :
            this.numFormat.transform(rd.valueRef, rd.format)) as any;
        }

      });
    }
  }

}
