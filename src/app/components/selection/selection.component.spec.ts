import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { SelectionComponent } from './selection.component';
// import { SelectionDropList } from '../../interfaces';

describe('SelectionComponent', () => {
  let component: SelectionComponent;
  let fixture: ComponentFixture<SelectionComponent>;
  let el: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectionComponent]
    })
      .compileComponents().then(() => {
        fixture = TestBed.createComponent(SelectionComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement.nativeElement;
        component.SelectionType = 'droplist';
        component.SelectionData = {
          labelInActive: 'Select an Initative',
          labelActive: 'Select an Initiative',
          options: [
            'Be Well, Work Well focus',
            'Leaders on board',
            'Managers of the future',
            'Bring in the specialists',
            'Transformation analysis',
            'Agile processes',
            'Automated alerts'
          ]
        };
        fixture.detectChanges();
      });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('On load label text', () => {
    expect(el.querySelector('.label').textContent).toContain('Select an Initative');
  });

  it('On click drop list open', () => {
    el.querySelector('.wrapper-dropdown').click();
    fixture.detectChanges();
    expect(el.querySelector('.label').textContent).toContain('Select an Initiative');
  });

  it('Select option', () => {
    spyOn(component.Selected, 'emit');
    el.querySelector('.wrapper-dropdown').click();
    fixture.detectChanges();
    el.querySelectorAll('li')[0].click();
    fixture.detectChanges();
    expect(el.querySelector('.label').textContent).toContain('Be Well, Work Well focus');
    expect(component.Selected.emit).toHaveBeenCalledTimes(1);
    expect(component.Selected.emit).toHaveBeenCalledWith('Be Well, Work Well focus');
  });

  it('On load Short select component should create and on click get selected data, should be limited selection feature that be configurable', fakeAsync(() => {
    component.SelectionType = 'short';
    component.SelectionData = {
      type: 'short',
      maxSelection: 5,
      options: [
        { title: 'Be Well, Work Well focus', article: 'Emphasize energy management and well-being across your project teams.' },
        { title: 'Leaders on board', article: 'Focus on involving PwC Partners and Directors more in team development.' },
        { title: 'Managers of the future', article: 'Attend a development program to enhance your skills on leading a team and getting the most out of your people.' },
        { title: 'Bring in the specilists', article: 'Invite PwC industry specialists to share sector-specific insights with the client and expand conversations.' },
        { title: 'Transformation analysis', article: 'Generate a PwC special report on challenges facing the client, customer insights and potential market opportunities.' },
        { title: 'Difficult conversations', article: 'Take a course to improve how you handle challenging client conversations.' },
        { title: 'Agile processes', article: 'Implement daily stand-ups, sprints and workflow processes to improve response to unexpected project developments.' },
        { title: 'Automated alerts', article: 'Invest in automated tracking processes to alert for budget crunches and milestone forecasting.' },
        { title: 'SDC boost', article: 'Get a headstart on SDC processes and communications.' },
        { title: 'Reinvest is best', article: 'Prioritize your reinvest opportunities to make an impact through business development, proposal writing, facilitating and other strategic initiatives.' },
        { title: 'Internal network', article: 'Build your network and brand accross PwC to enhance your personal growth. ' },
        { title: 'Communication effectiveness', article: 'Establish trust-based relationships with your clients early on, and enhance your interpersonal communication skills.' }
      ]
    };
    component.ngOnInit();
    if (component.SelectionData.type === 'short') {
      component.SelectionData.options.forEach(element => {
        element.selected = false;
      });
    }
    tick();
    expect(component.SelectionData.options[0].selected).toBe(false);
    fixture.detectChanges();

    expect(el.querySelectorAll('.short-option').length).toBe(12);


    spyOn(component.Selected, 'emit');
    el.querySelectorAll('.short-option')[0].click();
    fixture.detectChanges();
    component.Selected.emit();

    expect(component.Selected.emit).toHaveBeenCalledTimes(2);
    expect(component.Selected.emit).toHaveBeenCalledWith({ title: 'Be Well, Work Well focus', article: 'Emphasize energy management and well-being across your project teams.', selected: true, clickTitle: false });


    el.querySelectorAll('.short-option')[1].click();
    el.querySelectorAll('.short-option')[2].click();
    el.querySelectorAll('.short-option')[3].click();
    el.querySelectorAll('.short-option')[4].click();

    fixture.detectChanges();

    expect(el.querySelectorAll('.disable').length).toBe(7);

  }));

  it('On load Long select component should be create', fakeAsync(() => {
    component.SelectionType = 'long';
    component.SelectionData = {
      type: 'long',
      maxSelection: 5,
      options: [
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
        { title: '2020 Coins', article: [{ headline: '2020 Coins', description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.', cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }], benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }] }] },
        { title: 'Making it Better', article: [{ headline: 'Making it Better', description: 'Encourage the team to always be thinking about doing our best by asking the question "How do I make it better?" Hand out MIB hard hat stickers to those who do.', cost: [{ title: 'COST', price: '5 MTUs, $3M' }], benefit: [{ title: 'BENEFITS', income: 'Productivity, CSAT, Income' }] }] },
      ]
    };

    component.ngOnInit();
    if (component.SelectionData.type === 'short') {
      component.SelectionData.options.forEach(element => {
        element.selected = false;
        element.clickTitle = false;
      });
    }
    fixture.detectChanges();
    tick();
    expect(el.querySelectorAll('.long-option').length).toBe(16);

    spyOn(component.Selected, 'emit');
    el.querySelectorAll('.long-checkbox')[0].click();
    fixture.detectChanges();
    component.Selected.emit();
    tick();
    expect(component.Selected.emit).toHaveBeenCalledTimes(2);
    expect(component.Selected.emit).toHaveBeenCalledWith({
      title: '2020 Coins',
      article: [{
        headline: '2020 Coins',
        description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.',
        cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }],
        benefit: [{
          title: 'RESULTS',
          income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition'
        }]
      }],
      selected: false,
      clickTitle: true
    });


    el.querySelectorAll('.long-title')[0].click();
    el.querySelectorAll('.long-checkbox')[0].click();
    component.SelectionData.options.forEach(element => {
      element.forEach(ele => {
        ele.clickTitle = false;
      });
    });
    tick();
    expect(component.Selected.emit).toHaveBeenCalledWith({
      title: '2020 Coins',
      article: [
        {
          headline: '2020 Coins',
          description: 'When someone on your team does something outstanding, reward them with a coin recognizing that they embody the ideals and goals of Spectra 66 2020.',
          cost: [{ title: 'INVESTMENT', price: '5 MTUs, $5M' }],
          benefit: [{ title: 'RESULTS', income: 'CSAT, Lead Left Deals, Income, Cap Mkts/Treasury Penetration, Client acquisition' }]
        }],
      selected: true,
      clickTitle: false
    });

    el.querySelectorAll('.long-checkbox')[1].click();
    el.querySelectorAll('.long-checkbox')[2].click();
    el.querySelectorAll('.long-checkbox')[3].click();
    el.querySelectorAll('.long-checkbox')[4].click();
    el.querySelectorAll('.long-checkbox')[5].click();

    fixture.detectChanges();
    tick();
    expect(el.querySelectorAll('.long-option.disable').length).toBe(10);
  }));
});
