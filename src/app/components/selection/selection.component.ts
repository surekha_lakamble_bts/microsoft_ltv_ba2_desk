import { Component, OnInit, Input, Output, EventEmitter, ElementRef, AfterViewInit } from '@angular/core';
import * as Rx from 'rxjs/Rx';
import * as _ from 'lodash';

@Component({
  selector: 'ism-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.styl']
})
export class SelectionComponent implements OnInit {

  @Input() SelectionType: string;
  @Input() SelectionData: any;
  @Output() Selected = new EventEmitter();

  list = false;
  optionSelected: any = [];
  maxSelection: number;
  displayOptionCount: number;
  navigationFlag: string;

  constructor(private el: ElementRef) {
    const source = Rx.Observable.fromEvent(document, 'click');
    source.subscribe((d: any) => {
      const clickedInside = this.el.nativeElement.contains(d.target);
      if (!clickedInside) {
        this.list = false;
      }
    });
  }

  ngOnInit() {

    /**
     * Add selected property to short selection data
     */
    if (this.SelectionData.type === 'short' || this.SelectionData.type === 'long') {
      this.SelectionData.options.forEach(element => {
        element.selected = false;
        element.clickTitle = false;
      });
    }

    /**
     * Chunk for long selection
     */
    if (this.SelectionData.type === 'long') {
      this.displayOptionCount = this.SelectionData.options.length;
      this.SelectionData.options = _.chunk(this.SelectionData.options, 8);
      /* By default 1st option selected */
      this.SelectionData.options[0][0].selected = true;
      this.SelectedValue(this.SelectionData.options, null);
      this.shortOptionSelect(this.SelectionData.options[0][0]);
    }
  }

  droplist(event) {
    event.stopPropagation();
    this.list = !this.list;
  }

  SelectedValue(val, event, type?: string ) {
    this.Selected.emit(val);
    this.SelectionData.labelInActive = val;
    if (event !== null && type !== 'droplist') {
      event.stopPropagation();
    }
    if (this.SelectionData.type === 'long') {
      val.selected = !val.selected;

      const tempArray = [];
      Rx.Observable.from(this.SelectionData.options)
        .flatMap((f: any) => f)
        .filter((d: any) => {
          return d.selected === true;
        }).subscribe(data => {
          tempArray.push(data);
        });
      this.maxSelection = tempArray.length;
    }
  }

  shortOptionSelect(data) {
    if (this.SelectionData.type === 'long') {
      this.SelectionData.options.forEach(element => {
        element.forEach(ele => {
          ele.clickTitle = false;
        });
      });
      data.clickTitle = !data.clickTitle;
      this.optionSelected = data;

    } else {
      const index = this.SelectionData.options.indexOf(data);
      this.SelectionData.options[index].selected = !this.SelectionData.options[index].selected;
      this.maxSelection = this.SelectionData.options.filter(f => f.selected === true).length;
    }

  }

  displayOptions(flag) {
    this.navigationFlag = flag;
    return flag;
  }

}
