import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'ism-viewmode',
  templateUrl: './viewmode.component.html',
  styleUrls: ['./viewmode.component.styl']
})
export class ViewmodeComponent implements OnInit {

  @Input() switchView: string;
  @Output() viewSize: EventEmitter<string> = new EventEmitter();
  @ViewChild('eleWidth') elementWidth: any;

  constructor() {
    this.selectDashboard(document.body.clientWidth);
  }

  ngOnInit() {
    this.selectDashboard(document.body.clientWidth);
  }

  selectDashboard(data) {
    if (data > 1920) {
      this.switchView = 'wide';
      this.viewSize.emit(this.switchView);
    } else {
      this.switchView = 'standard';
      this.viewSize.emit(this.switchView);
    }
  }

  onResize(event) {
    this.selectDashboard(event.srcElement.innerWidth);
  }

}
