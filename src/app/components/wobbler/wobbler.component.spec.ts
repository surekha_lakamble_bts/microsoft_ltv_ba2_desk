import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { WobblerComponent } from './wobbler.component';

describe('WobblerComponent', () => {
  let component: WobblerComponent;
  let fixture: ComponentFixture<WobblerComponent>;
  let el: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WobblerComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WobblerComponent);
    component = fixture.componentInstance;
    el = fixture.nativeElement;
    component.showWobbler = true;
    component.wobblerData = {
      timer: 100,
      title: 'Retail Distribution Model Disruption',
      description: 'I was just at one of our largets customers, All Auto Co, they want to update their Enterprise hardware. I know we can fill their need and probably even get the sale in this quarter. I think if we can addin our software production to the sale, it will be a better solution for All Auto Co and ultimately more revenue for us. ',
      background: 'assets/images/background.jpg',
      reviewBtntext: 'Review Decision',
      closeReviewBtnBtn: 'Close Review',
      feedbackOne: `<div>
       <p>Fusce laoreet sapien a magna porta, vitae tristique eros faucibus. In id fringilla est, vel ornare massa. Vivamus pharetra lacus in erat tincidunt, a maximus risus pulvinar. Curabitur interdum eros at suscipit auctor. Morbi lobortis luctus augue, sed ornare ex auctor eu.</p>
       <p>Maecenas eget lorem a orci blandit facilisis et eget velit. Cras sed dictum justo, ac feugiat purus. Aliquam pellentesque rhoncus ullamcorper. Phasellus elementum sodales erat, quis mattis mauris tincidunt eu. Aenean nec imperdiet lacus, sed dignissim purus. Curabitur sit amet lacus non arcu consequat molestie in sit amet nibh. Duis tempus, ex at congue convallis, tortor ex vehicula nisl, eu scelerisque lorem ante vel massa.</p>
      </div>`,
      feedbackTwo: `
      <p>
      I don't like to admit it, but we've been losing more and more bids to our competitor Best Machines lately. My team has done some analysis and determined that it comes down to three key factors:</p>
      <p>1) We're more expensive;</p>
      <p>2) While our products are great in many ways, their products beat us out on availability, which is increasingly important to the customers;</p>
      <p>3) Our bids are just not very compelling and our graphics and presentations need a lot of work. </p>
      <p>My recommendation is that we focus on improving our competitive position in one of these areas immediately. What area should we focus on to beat Best Machines in the market?</p>
      `,
      feedbackThree: `
      <p>This was a significant investment but we're seeing some very positive results. Customers have been impressed with our offerings, now that we can finally show them what we can do. They're also been very pleased with our quick turnaround times not only on initial bids, but on any subsequent revisions.</p>
      `,
      options: [
        { title: '<b>A:</b> Sell the Hardware', desc: 'Times are tough right now and we can not afford to miss the opportunity to establish a relationship with a new customer. We also could really use the new revenue stream. We should just sell the hardware product with the competitor\'s OS, if that is what the customer really wants.' },
        { title: '<b>B:</b> Sell the Solution', desc: 'Times are tough right now and we can not afford to miss the opportunity to establish a relationship with a new customer. We also could really use the new revenue stream. We should just sell the hardware product with the competitor\'s OS, if that is what the customer really wants.' },
        { title: '<b>C:</b> Another Title', desc: 'Times are tough right now and we can not afford to miss the opportunity to establish a relationship with a new customer. We also could really use the new revenue stream. We should just sell the hardware product with the competitor\'s OS, if that is what the customer really wants.' }
      ]
    };
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('On click options and Review Decision', fakeAsync(() => {
    /** Click on first option */
    el.querySelectorAll('.options>div')[0].click();
    fixture.detectChanges();
    tick();
    expect(el.querySelector('button.active-btn').innerText).toContain('Submit');

    /** Click on Submit Button */
    el.querySelector('button.active-btn').click();
    fixture.detectChanges();
    tick();
    expect(el.querySelector('.back').className).toContain('back');

    /** Click on Submit Button */
    el.querySelector('.search-tab').click();
    fixture.detectChanges();
    tick();
    expect(el.querySelector('button.active-btn').className).toContain('active-btn');

    /** Click on Continue button and wobbler disappeared */
    el.querySelector('.continue-btn').click();
    fixture.detectChanges();
    tick();
    expect(el.querySelector('.back').style.visibility).toBeFalsy();
  }));
});
