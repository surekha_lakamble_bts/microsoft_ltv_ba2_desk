import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ism-wobbler',
  templateUrl: './wobbler.component.html',
  styleUrls: ['./wobbler.component.styl']
})
export class WobblerComponent implements OnInit {

  @Input() wobblerData: any = {};
  @Output() optionSelected = new EventEmitter<boolean>();

  activeBtn = false;
  reviewedData: string;
  newReviewData: string = null;
  showWobbler = false;
  reviewBtnSwitch = true;
  constructor() { }

  ngOnInit() {

    setTimeout(() => {
      this.showWobbler = true;
    }, this.wobblerData.timer);
    // Reset Data
    this.dataReset('all');
  }

  dataReset(flag) {
    this.wobblerData.options.forEach(element => {

      switch (flag) {
        case 'all':
          element.active = false;
          element.reviewOptionActive = false;
          break;
        case 'active':
          element.active = false;
          break;
        case 'reviewOptionActive':
          element.reviewOptionActive = false;
          break;
      }

    });
  }
  selectOption(index) {
    this.activeBtn = true;
    // Reset Data
    this.dataReset('active');

    this.wobblerData.options[index].active = !this.wobblerData.options[index].active;
  }

  reviewSelectOption(index) {
    // Reset Data
    this.dataReset('reviewOptionActive');
    this.wobblerData.options[index].reviewOptionActive = !this.wobblerData.options[index].reviewOptionActive;
    this.newReviewData = this.wobblerData.options[index];
  }

  submit() {
    this.wobblerData.options.forEach(element => {
      if (element.active) {
        this.reviewedData = element;
      }
    });
    const dom = document.querySelector('.flipper') as any;
    // Code for Safari
    dom.style.WebkitTransform = 'rotateY(-180deg)';
    // Code for IE9
    dom.style.msTransform = 'rotateY(-180deg)';
    // Standard syntax
    dom.style.transform = 'rotateY(-180deg)';
    // For IE 11
    const doc = document as any;
    if ((navigator.userAgent.indexOf('MSIE') !== -1) || (!!doc.documentMode === true)) {
      const front = document.querySelector('.front') as any;
      front.style.WebkitBackfaceVisibility = 'visible';
      front.style.msBackfaceVisibility = 'visible';
      front.style.backfaceVisibility = 'visible';

      front.style.WebkitTransform = 'none';
      front.style.msTransform = 'none';
      front.style.transform = 'none';
      front.style.height = '50px';
      front.style.overflow = 'hidden';

      const back = document.querySelector('.back') as any;
      back.style.WebkitBackfaceVisibility = 'visible';
      back.style.msBackfaceVisibility = 'visible';
      back.style.backfaceVisibility = 'visible';

    }
  }

  backSubmit() {
    const dom = document.querySelector('.flipper') as any;
    // Code for Safari
    dom.style.WebkitTransform = 'rotateY(-0deg)';
    // Code for IE9
    dom.style.msTransform = 'rotateY(-0deg)';
    // Standard syntax
    dom.style.transform = 'rotateY(-0deg)';
    // For IE 11
    const doc = document as any;
    if ((navigator.userAgent.indexOf('MSIE') !== -1) || (!!doc.documentMode === true)) {
      const front = document.querySelector('.front') as any;
      front.style.WebkitTransform = 'rotateY(0deg)';
      front.style.msTransform = 'rotateY(0deg)';
      front.style.transform = 'rotateY(0deg)';
      front.style.height = 'auto';
      front.style.overflow = 'visible';

      const back = document.querySelector('.back') as any;
      back.style.WebkitBackfaceVisibility = 'hidden';
      back.style.msBackfaceVisibility = 'hidden';
      back.style.backfaceVisibility = 'hidden';

    }
  }

  reviewDecision() {
    this.reviewBtnSwitch = !this.reviewBtnSwitch;
  }

  decisionSubmit() {
    const maincontainer = document.querySelector('.wobbler') as any;
    maincontainer.style.display = 'none';
    maincontainer.style.overflow = 'hidden';
    this.optionSelected.emit(true);

  }

}
