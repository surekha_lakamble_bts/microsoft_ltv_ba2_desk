import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NavBarComponent } from './nav-bar.component';
import { By } from '@angular/platform-browser';
import { DecisionComponent, SampleComponent } from '../../views/views-routing.module';
import { Routes } from '@angular/router';
import { IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { FormsModule } from '@angular/forms';

describe('NavBarComponent', () => {
  let component: NavBarComponent;
  let fixture: ComponentFixture<NavBarComponent>;
  const routes: Routes = [
    {
      component: SampleComponent,
      path: 'sample'
    },
    {
      component: DecisionComponent,
      path: 'decision'
    }];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), IsomerCoreModule, FormsModule],
      declarations: [NavBarComponent, DecisionComponent, SampleComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Set horizontal menu
    component.barType = 'hr';

    // Data for menu links
    component.navData = [
      { titleRef: 'decision', route: 'decision' },
      { titleRef: 'report', route: 'report' },
      { titleRef: 'Dropdown Link 1', subNav: [{ titleRef: 'test', route: 'test' }, { titleRef: 'Link 2', route: 'link' }] },
      { titleRef: 'Dropdown Link 2', subNav: [{ titleRef: 'link11', route: 'link11' }, { titleRef: 'sample', route: 'sample' }] }
    ] as any;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOninIt', () => {
    expect(component.activeMainNavIndex).toBe(-1);
    localStorage.setItem('activeMainNavIndex', '1');
    fixture.detectChanges();
    component.ngOnInit();
    expect(component.activeMainNavIndex).toBe(1);
  });

  it('should call showNavigation on click main link', fakeAsync(() => {
    // Click on dropdown link
    const link1 = fixture.debugElement.nativeElement.querySelectorAll('.left-div>ul>li>a')[2];
    link1.click();

    expect(component.SubNav.length).toBe(2);

    // Click on simple link
    const link2 = fixture.debugElement.nativeElement.querySelectorAll('.left-div>ul>li>a')[0];
    link2.click();

    tick();
    expect(component.activeMainNavIndex).toBe(-1);

  }));

  it('should call showSubNavigation on click sub link', fakeAsync(() => {
    // Click on dropdown link
    const link2 = fixture.debugElement.nativeElement.querySelectorAll('.left-div>ul>li>a')[3];
    link2.click();
    fixture.detectChanges();

    // Click on dropdown sub  link
    const link1 = fixture.debugElement.nativeElement.querySelectorAll('.right-div>ul>li>a')[3];
    link1.click();

    tick();
    expect(component.activeMainNavIndex).not.toBe(-1);

  }));

});
