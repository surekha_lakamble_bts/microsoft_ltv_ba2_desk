import { Component, OnInit, Input, ViewChild, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { StorageService } from '@btsdigital/ngx-isomer-core';
import { NavBar } from '../../interfaces';
// import '../../../script/dropdown'

/**
 * NavBar component renders the navigation bar based on the provided input
 * Navigation bar is rendered using ism-nav-bar component of ngx-isomer-core
 *
 */
@Component({
    selector: 'ism-nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.styl'],
    encapsulation: ViewEncapsulation.Emulated
})
export class NavBarComponent implements OnInit, OnDestroy {
    /**
     * showSubNav is for switch the class
     */
    showSubNav = false;

    /**
     * animate is for show and hide the element
     */
    animate = false;

    /**
     * SubNav is used to save the current clicked state of navigation.
     */
    SubNav: NavBar[] = [];

    /**
     * Input binding for horizontal or vertical type flag.
     */
    @Input() barType: string;

    /**
     * Input binding for Navigation Data
     */
    @Input() navData: string[];

    /**
   * Constructor for nav-bar component
   *
   * @param {StorageService} storage StorageService instance
   *
   * @param {Router} router Router instance
   *
   */
    constructor(private router: Router, private storage: StorageService) { }

    activeMainNavIndex = -1;

    ngOnInit() {
        if (this.storage.getValue('activeMainNavIndex')['__zone_symbol__value']) {
            this.activeMainNavIndex = Number(this.storage.getValue('activeMainNavIndex')['__zone_symbol__value']);
        }
    }

    /**
     * On click main link and dropdown
     */
    showNavigation(data: NavBar) {
        if (data.subNav) {
            this.SubNav = data.subNav;
            this.showSubNav = true;
            this.animate = true;
        } else {
            this.activeMainNavIndex = -1;
            this.storage.setValue('activeMainNavIndex', this.activeMainNavIndex + '');
        }
    }

    /**
     * On click dropdown sub link
     */
    showSubNavigation(data: NavBar) {
        let i = 0;
        // Find the index of parent link
        _.each(this.navData, (x) => {
            const foundIndex = _.findIndex(x['subNav'], data);
            if (foundIndex >= 0) {
                this.activeMainNavIndex = i;
                this.storage.setValue('activeMainNavIndex', this.activeMainNavIndex + '');
                return;
            }
            i++;
        });
    }

    /**
    * Removes storage when component instance is destroyed
    *
    */
    ngOnDestroy() {
        this.storage.clear('activeMainNavIndex');
    }

}
