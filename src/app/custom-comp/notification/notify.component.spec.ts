import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DecisionComponent, SampleComponent } from '../../views/views-routing.module';
import { Routes } from '@angular/router';
import { IsomerCoreModule } from '@btsdigital/ngx-isomer-core';
import { NotifyComponent, NotifyService, SharedNotifyModule } from './notify.component';

describe('NotifyComponent', () => {
    let component: NotifyComponent;
    let fixture: ComponentFixture<NotifyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [NotifyComponent],
            providers: [NotifyService]
        }).compileComponents();
        fixture = TestBed.createComponent(NotifyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should call showNotify on click bell icon', () => {
        const bell = fixture.debugElement.nativeElement.querySelector('.notify-bell');
        bell.click();
        expect(component.show).toBeTruthy();

        bell.click();
        expect(component.show).toBeFalsy();
    });

});
