import { Component, NgModule, OnInit, Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { CommonModule } from '@angular/common';

/**
 * NotifyService is responsible for notify to NotifyComponent about the message.
 *
 */
@Injectable()
export class NotifyService {
    /**
     * Notification data instance.
     */
    public notificationData = { flag: false, message: '', display: false };
}

/**
 * Notify component renders the notification based on the provided input
 * Notify is rendered using ism-notify-comp component of ngx-isomer-core
 *
 */
@Component({
    selector: 'ism-notify-comp',
    templateUrl: 'notify.component.html',
    styleUrls: ['./notify.component.styl']
})
export class NotifyComponent implements OnInit {
    /**
     * data is for containing the data from bootstrap service.
     */
    data: any;

    /**
     * show is for switch the class
    */
    show: boolean;

    /**
   * Constructor for notify-comp component
   *
   * @param {NotifyService} key NotifyService instance
   *
   */
    constructor(private key: NotifyService) {

    }
    ngOnInit() {
        this.data = this.key.notificationData;
    }

    showNotify() {
        this.show = !this.show;
    }

}

/**
 * Notification module is used to notify about model loading.
 *
 */
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        NotifyComponent
    ],
    providers: [NotifyService],
    exports: [
        NotifyComponent,
    ]
})
export class SharedNotifyModule { }
