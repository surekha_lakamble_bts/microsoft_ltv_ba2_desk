
/**
 * Interface for NavBar configuration
 */
export interface NavBar {
    titleRef: string;
    icon?: string;
    route?: string;
    subNav?: NavBar[];
    isOpen?: boolean;
    accordianId?: string;
}

/**
 * Interface for ChartData configuration
 */
export interface ChartData {
    chartOptions?: any;
    chartTitle?: string;
    rangeRef: any;
    seriesOptions?: any;
    xAxis?: any;
    yAxis?: any;
}

/**
 * Interface for MetricTableValueObject configuration
 */
export interface MetricTableValueObject {
    colspanClass?: string;
    format?: string;
    fontWeight?: string | number;
    color?: string;
    isHighlighted?: boolean;
    ref: string;
    scaler?: any;
    showNegativeBalloon?: boolean;
    showPositiveBalloon?: boolean;
    textAlignClass?: string;
    yearRef?: string;
}

/**
 * Interface for MetricTableRowData configuration
 */
export interface MetricTableRowData {
    rowData: MetricTableValueObject[];
}

/**
 * Interface for MetricTableSlideData configuration
 */
export interface MetricTableSlideData {
    slideData: MetricTableRowData[];
}

/**
 * Interface for MetricTableData configuration
 */
export interface MetricTableData {
    tableData: MetricTableSlideData[];
    withCarousel?: boolean;
    wideScreen?: boolean;
}

/**
 * Interface for DashboardGroupData configuration
 */
export interface DashboardGroupData {
    accordianOptions?: NavBar;
    chartData?: ChartData[];
    metricTable?: MetricTableData;
}

/**
 * Interface for Selection - Drop List
 */
export interface Selection {
    type: string;
    labelInActive?: string;
    labelActive?: string;
    maxSelection?: number;
    options: string[] | any[];
}

/**
 * Interface for ReportTable configuration
 */
export interface ReportTable {
    keyRef?: string;
    valueRef?: string;
    keyYearRef?: string;
    valueYearRef?: string;
    keyFontStyle?: string;
    valueFontStyle?: string;
    keyFontWeight?: any;
    valueFontWeight?: any;
    highlightRow?: boolean;
    isLine?: boolean;
    format?: string;
    scaler?: any;
}

/**
 * Interface for Tab Configuration
 */
export interface Tab {
    title?: string;
    isactive?: boolean;
    tabContent?: Object;
}
