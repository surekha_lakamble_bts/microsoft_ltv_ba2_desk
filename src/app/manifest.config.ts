// set the configuration for offline isomer
import { environment } from '../environments/environment';

const hostName: string = (environment.CDN0LOCAL1LOCALHOST2 === 0 ?
    'https://' + environment.hostname + '/' : (environment.CDN0LOCAL1LOCALHOST2 === 1 ?
        window.location.protocol + '//' + window.location.hostname : 'http://localhost'));

export const manifest = {
    hostName: hostName,
    serviceUrl: hostName + '/PulseServices/',
    eventTitle: 'MS_LifeTimeValue_BA2',
    questionsToSend: [
        { questionName: 'dec_TeamName', rangeName: 'dec_TeamName' }
    ],
    questionsToReceive: [
        { questionName: 'dec_TeamNumber', rangeName: 'dec_TeamNumber' },
    ],
    foremanquestionsToRecieve: [],
    trackQuestion: null
};