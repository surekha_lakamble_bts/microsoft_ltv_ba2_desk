define(['./MSFT_LTV_BA2-require', './TRCourseActions', 'module'], function(model, customActions, module) {
    module.exports = {
        model: model,
        customActions: customActions,
        modelName: 'MSFT_LTV_BA2-require'
    }
});