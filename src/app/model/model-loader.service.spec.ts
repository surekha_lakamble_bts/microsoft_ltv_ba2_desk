import { inject, TestBed } from '@angular/core/testing';
import { Http, HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CalcService, CommunicatorService, HttpWrapperService, StorageService, TextService, SyncService, IsomerCoreModule, JsCalcConnectorService } from '@btsdigital/ngx-isomer-core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { SharedNotifyModule } from '../custom-comp/notification/notify.component';
import { BootstrapService } from '../shared/bootstrap.service';
import { ModelLoaderService } from './model-loader.service';
import { LoggerService } from '@btsdigital/pulseutilities';
import { VideoCallManagerService, VideoCallStateManagerService } from '@btsdigital/pulsewebrtc';
import { OpentokService, OpentokConfig } from 'ng2-opentok';
import { SignalRService } from '@btsdigital/pulsesignalr';

declare var require: any;
const _model: any = require('./index');
// var httpWrapperService: HttpWrapperService;
let http: Http;

describe('ModelLoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpModule, SharedNotifyModule, IsomerCoreModule
      ],
      providers: [
        ModelLoaderService, CalcService, CommunicatorService, LoggerService, StorageService, HttpWrapperService,
        TextService, BootstrapService,
        ModelLoaderService,
        VideoCallManagerService,
        VideoCallStateManagerService,
        OpentokService,
        OpentokConfig,
        SignalRService,
        SyncService, JsCalcConnectorService
      ],
    }).compileComponents();
    // httpWrapperService = TestBed.get(HttpWrapperService);
    http = TestBed.get(Http);
  });

  it('should be created', inject([ModelLoaderService], (mlService: ModelLoaderService) => {
    expect(mlService).toBeTruthy();
  }));

  // Note: To call the http request and get response, we should use httpWrapperService but because its
  // returning the unwrapped body and we are not getting the full json response, so we are now
  // using normal http request and spy on that.
  it('Model Data Loading from API', inject([BootstrapService, ModelLoaderService],
    (bss: BootstrapService, mlService: ModelLoaderService) => {

      // spyOn(httpWrapperService, 'postJson').and.callFake(() => Promise.resolve({ success: true }));
      spyOn(mlService, 'sendModelRequest').and.callFake(() => Promise.resolve({ success: true }));

      const result: any = mlService.sendModelRequest(_model.modelName);

      if (result) {
        console.log('Data is fetching from API.');
        expect(result.__zone_symbol__value.success).toBe(true, 'Data is fetching from API.');
      }
    }));

  it('Model Data Loading from Build', inject([BootstrapService, ModelLoaderService],
    (bss: BootstrapService, mlService: ModelLoaderService) => {
      if (_model && _model.modelName) {
        console.log('Data is fetching from Build.');
        expect(_model.modelName !== '').toBe(true, 'Data is fetching from Build.');
      }
    }));

});
