import { SafeHtmlPipe } from './safe-html.pipe';
import { TestBed, inject } from '@angular/core/testing';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';

// describe('SafeHtmlPipe', () => {
//   it('create an instance', () => {
//     const pipe = new SafeHtmlPipe('aa');
//     expect(pipe).toBeTruthy();
//   });
// });

describe('SanitiseHtmlPipe', () => {
  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [
          BrowserModule
        ]
      });
  });

  it('create an instance', inject([DomSanitizer], (domSanitizer: DomSanitizer) => {
    const pipe = new SafeHtmlPipe(domSanitizer);
    expect(pipe).toBeTruthy();
  }));

});
