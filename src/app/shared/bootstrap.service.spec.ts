import { async, inject, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CalcService, CommunicatorService, HttpWrapperService, IsomerCoreModule, JsCalcConnectorService, StorageService, SyncService, TextService } from '@btsdigital/ngx-isomer-core';
import { SignalRService } from '@btsdigital/pulsesignalr';
import { LoggerService } from '@btsdigital/pulseutilities';
import { VideoCallManagerService, VideoCallStateManagerService } from '@btsdigital/pulsewebrtc';
import { OpentokConfig, OpentokService } from 'ng2-opentok';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { SharedNotifyModule } from '../custom-comp/notification/notify.component';
import { ModelLoaderService } from '../model/model-loader.service';
import { BootstrapService } from './bootstrap.service';

describe('BootstrapService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule, RouterTestingModule, SharedNotifyModule, IsomerCoreModule],
      providers: [
        BootstrapService, CalcService, CommunicatorService, LoggerService, StorageService, HttpWrapperService,
        TextService, ModelLoaderService, VideoCallManagerService,
        VideoCallStateManagerService, OpentokService,
        OpentokConfig,
        SignalRService,
        SyncService, JsCalcConnectorService
      ],
    }).compileComponents();
  }));
  it('should be created', inject([BootstrapService], (service: BootstrapService, calcService: CalcService) => {
    expect(service).toBeTruthy();
  }));
});
