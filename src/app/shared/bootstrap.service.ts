import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CalcService, CommunicatorService, Constants, HttpWrapperService, ManifestService, StorageService, SyncService, TextService } from '@btsdigital/ngx-isomer-core';
import { SignalRService } from '@btsdigital/pulsesignalr';
import { LoggerService } from '@btsdigital/pulseutilities';
import { VideoCallManagerService } from '@btsdigital/pulsewebrtc';
import { environment } from '../../environments/environment';
import { NotifyService } from '../custom-comp/notification/notify.component';
// import { environment } from '../../environments/environment';
import { manifest } from '../manifest.config';
import { ModelLoaderService } from '../model/model-loader.service';

window['__theme'] = 'bs4';
@Injectable()
export class BootstrapService {

  // private modelLoadProgressSub: Subscription;
  private textServiceInitPromise: Promise<any>;
  private modelLoadCompletePromise: Promise<any>;
  private isAppReady = false;
  // private testNotify = new notifyComponent();
  constructor(private calcService: CalcService, private textService: TextService,
    private storageService: StorageService, private httpWrapper: HttpWrapperService,
    private communicator: CommunicatorService, private logger: LoggerService, private videoCallManager: VideoCallManagerService,
    private router: Router, private modelLoader: ModelLoaderService, private signalR: SignalRService, private syncService: SyncService,
    private testNotify: NotifyService, private manifestService: ManifestService, private httpWrapperService: HttpWrapperService) { }

  init(): Promise<any> {
    return this.modelLoader.getModel().then((response) => {
      const model = response;

      // set theme mode
      // set storage mode
      this.storageService.setMode(environment.connectToPulse ? Constants.STORAGE_MODES.MIXED : Constants.STORAGE_MODES.LOCAL);
      // set initial language to be loaded
      this.textService.language = 'en';
      this.testNotify.notificationData['flag'] = true;
      this.testNotify.notificationData['message'] = 'Model Loaded Successfully';
      this.testNotify.notificationData['display'] = true;
      this.manifestService.setConfig({config: manifest});
      this.httpWrapperService.setHostName(manifest.hostName);
      this.signalR.configureSignalRService({ hostname: manifest.hostName, serviceUrl: manifest.serviceUrl, hasHeartBeat: true });
      this.videoCallManager.configureWebRTCService({ serviceUrl: manifest.serviceUrl, eject: false });

      // load text content
      this.textServiceInitPromise = this.textService.init();
      // try and load the calc model
      this.modelLoadCompletePromise = this.calcService
        .getApi(model)
        .catch((err) => {
          this.logger.log('Error when loading model ', err);
          this.testNotify.notificationData['flag'] = false;
          this.testNotify.notificationData['message'] = err;
          this.testNotify.notificationData['display'] = true;
        });

      return Promise.all([this.textServiceInitPromise, this.modelLoadCompletePromise])
        .then(() => {
          this.isAppReady = true;
          if (environment.connectToPulse === true) {
            this.syncService.initializeSync().then(() => {
              this.syncService.setMode(Constants.CONNECTION_MODE.PUSH);
            });
          }
        });
    });
  }

  // private onProgress(progress: string) {
  //   this.logger.log('Progress ' + progress);
  // }

  public set appReady(v: boolean) {
    this.isAppReady = v;
  }

  public get appReady(): boolean {
    return this.isAppReady;
  }
}
