import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CalcService, CommunicatorService, HttpWrapperService, StorageService, TextService } from '@btsdigital/ngx-isomer-core';
import { LoggerService } from '@btsdigital/pulseutilities';
import { ModelLoaderService } from '../../model/model-loader.service';
import { BootstrapService } from '../bootstrap.service';
import { ExternalModelComponent } from './external-model.component';

describe('ExternalModelComponent', () => {
  // var component: ExternalModelComponent;
  let fixture: ComponentFixture<ExternalModelComponent>;
  // var nativeElement: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, HttpModule
      ],
      declarations: [
        ExternalModelComponent
      ],
      providers: [
        BootstrapService, CalcService, CommunicatorService, LoggerService, StorageService, HttpWrapperService,
        TextService, ModelLoaderService
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    fixture = TestBed.createComponent(ExternalModelComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('Model Data Loading', (done) => {
    inject([ModelLoaderService],
      (mlService: ModelLoaderService) => {
      const emc = new ExternalModelComponent(mlService);
      mlService.getModel().then((response) => {
        const model = response;

        if (model) {
          expect(model.modelName).toBe('simpleWorkbook-require', 'model data found');
        }

        done();
      });
    })();
  });

});
