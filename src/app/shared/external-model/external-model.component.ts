import { Component, OnInit } from '@angular/core';
import { Input, Output, ElementRef, ViewChild } from '@angular/core';
import { saveAs } from 'file-saver/FileSaver';
import { ModelLoaderService } from '../../model/model-loader.service';

declare var require: any;
declare var nw: any;

@Component({
    selector: 'ism-external-model',
    templateUrl: './external-model.component.html',
    styleUrls: ['./external-model.component.styl']
})

export class ExternalModelComponent implements OnInit {
    model = null;
    filestream = null;
    fileName = '';
    isoffline = false;
    showUpload = false;
    showConfirm = false;

    constructor(private modelLoader: ModelLoaderService) { }

    ngOnInit() {
        this.showUpload = false;

        if (typeof(nw) !== 'undefined') {
            this.isoffline = true;
        }

        return this.modelLoader.getModel().then((response) => {
            this.model = response;
        });
    }

    onFileSelect(fileEvent) {
        this.filestream = fileEvent.target.files[0];
        this.fileName = this.filestream.name;

        if (this.fileName) {
            this.showConfirm = true;
        }
    }

    onModelUpload() {
        const self = this;
        const reader: FileReader = new FileReader();
        const path = require('path');
        const fs = nw.require('fs');

        reader.onload = function(e) {
            // const blobx = new Blob([e.target['result']], { type: 'text/plain' });
            // saveAs(blobx, filePath);

            const blobx = e.target['result'];
            const filePath = path.join(nw.App.dataPath, self.fileName);

            fs.writeFile(filePath, blobx, function (err) {
                if (err) {
                    console.log('There was an error attempting to save your data.');
                    console.log(err.message);
                    alert('There was an error attempting to save your data.');
                    return;
                }

                self.showUpload = false;
                self.showConfirm = false;
                console.log('File has been saved.');

                self.ngOnInit();
            });
        };

        reader.onerror = function(e) {
            console.log('File Error!');
        };

        reader.readAsText(this.filestream);

        this.showConfirm = false;
    }

    onCancelUpload() {
        this.showUpload = false;
        this.showConfirm = false;
    }
}
