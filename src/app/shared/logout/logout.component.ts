import { Component, OnInit, OnDestroy } from '@angular/core'
import { HttpWrapperService } from '@btsdigital/ngx-isomer-core'
import { Router } from '@angular/router';

@Component({
    template:`<ng-content></ng-content>`
})

export class LogoutComponent implements OnInit, OnDestroy {
    
    constructor(private httpWrapper: HttpWrapperService, private router: Router) {
    }

    ngOnInit() {
        let logoutUrl = (process.env.CDN0LOCAL1LOCALHOST2 === '0') ? '/Wizer/CloudFront/Logout' : '/Wizer/Wizer/Logout';
        this.httpWrapper.postJson(logoutUrl, null)
        .then(result => {
            // clear all cookies
            var cookies = document.cookie.split(';')
            for(var i = 0; i<cookies.length; i++) {
                var cookie = cookies[i].split('=')[0].trim()
                if (cookie.length > 0) {
                    document.cookie = cookie + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC"
                }
            }

            setTimeout(()=>{
                if (process.env.CDN0LOCAL1LOCALHOST2 == '1') {
                    window.location.href = window.location.protocol + "//" + window.location.hostname + "/sim/login/";
                }
                else {
                    window.location.href = window.location.protocol + "//" + window.location.hostname + ((window.location.port) ? ":" + window.location.port : "") + "/";
                }                
            },500)
        })
    }

    ngOnDestroy() {
    }
}