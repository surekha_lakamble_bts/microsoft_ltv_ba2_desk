import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SplashComponent } from './splash/splash.component';
import { LogoutComponent } from './logout/logout.component';
import { ModelLoaderService } from '../model/model-loader.service';
import { BootstrapService } from './bootstrap.service';
import { IsomerCoreModule, JsCalcConnectorService } from '@btsdigital/ngx-isomer-core';
import { BsDropdownModule, AlertModule, ModalModule,
  AccordionModule, ButtonsModule, CarouselModule,
  TabsModule, TooltipModule, PopoverModule } from 'ngx-bootstrap';
import { SharedNotifyModule, NotifyComponent } from '../custom-comp/notification/notify.component';
import { WebRTCModule } from '@btsdigital/pulsewebrtc';
import { ComponentsModule } from '../components/components.module';
// import { SharedNotifyModule } from '../custom-comp/notification/notify.component';
import { LoggerService as PulseLoggerService } from '@btsdigital/pulseutilities';


@NgModule({
  imports: [
    CommonModule,
    IsomerCoreModule,
    BsDropdownModule.forRoot(),
    AlertModule.forRoot(),
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    ButtonsModule.forRoot(),
    CarouselModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    PopoverModule.forRoot(),
    WebRTCModule,
    ComponentsModule,
    SharedNotifyModule
  ],
  declarations: [SplashComponent, LogoutComponent],
  providers: [ ModelLoaderService, BootstrapService, PulseLoggerService, JsCalcConnectorService ],
  exports: [
    SplashComponent, LogoutComponent,
    IsomerCoreModule, WebRTCModule, ComponentsModule, SharedNotifyModule,
    AlertModule, BsDropdownModule, ModalModule, AccordionModule,
    ButtonsModule, CarouselModule, TabsModule,
    TooltipModule, PopoverModule
  ]
})
export class SharedModule { }
