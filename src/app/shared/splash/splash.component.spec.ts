import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { CalcService, CommunicatorService, HttpWrapperService, IsomerCoreModule, JsCalcConnectorService, NumberFormattingPipe, StorageService, SyncService, TextService } from '@btsdigital/ngx-isomer-core';
import { SignalRService } from '@btsdigital/pulsesignalr';
import { LoggerService } from '@btsdigital/pulseutilities';
import { VideoCallManagerService, VideoCallStateManagerService } from '@btsdigital/pulsewebrtc';
import { OpentokConfig, OpentokService } from 'ng2-opentok';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { SharedNotifyModule } from '../../custom-comp/notification/notify.component';
import { ModelLoaderService } from '../../model/model-loader.service';
import { BootstrapService } from '../bootstrap.service';
import { SplashComponent } from './splash.component';

describe('SplashComponent', () => {
  let component: SplashComponent;
  let fixture: ComponentFixture<SplashComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SplashComponent],
      imports: [HttpModule, RouterTestingModule, SharedNotifyModule, IsomerCoreModule],
      providers: [
        ModelLoaderService,
        BootstrapService,
        BootstrapService,
        CalcService,
        CommunicatorService,
        LoggerService,
        StorageService,
        HttpWrapperService,
        TextService,
        VideoCallManagerService,
        VideoCallStateManagerService, OpentokService,
        OpentokConfig,
        SignalRService,
        SyncService, JsCalcConnectorService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    // create component and test fixture
    fixture = TestBed.createComponent(SplashComponent);
    // get test component from the fixture
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
