import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BootstrapService } from '../bootstrap.service';
import { StorageService, Constants, CommunicatorService } from '@btsdigital/ngx-isomer-core';
import { Subscription } from 'rxjs/Subscription';
import { LoggerService } from '@btsdigital/pulseutilities';

@Component({
  selector: 'ism-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.styl']
})
export class SplashComponent implements OnInit {

  private modelProgressSubscription: Subscription;
  progress = 0;
  progressPercentage: any = 0;
  progressMessage: string = "Loading model...";

  constructor(private bootstraper: BootstrapService, private router: Router,
    private logger: LoggerService, private storageService: StorageService,
    private communicator: CommunicatorService) { }

  ngOnInit() {
    if (!this.bootstraper.appReady) {
      this.modelProgressSubscription = this.communicator
        .getEmitter(Constants.MODEL_LOAD_PROGRESS)
        .subscribe((progress) => {
          this.progress = progress;
          if (this.progress > 1) {
            this.progressPercentage = this.progress / 100;
          }
          else {
            this.progressPercentage = this.progress * 100;
          }
          this.progressPercentage = parseInt(this.progressPercentage.toString());
          if (parseInt(this.progress + "") === 1) {
            this.progressMessage = "Initializing model...";
          } else {
            this.progressMessage = "Loading model...";
          }
        });

      this.bootstraper
        .init()
        .then(() => {
          this.appReady();
        });
    }
  }

  private appReady() {
    this.logger.log('Model loaded');
    let returnUrl: string;
    this.storageService.getValue(Constants.RETURN_URL)
      .then(val => {
        returnUrl = ((val && val != '/dashboard') ? val : '/dashboard/feature').toString();
        this.router
          .navigateByUrl(returnUrl);
      })
      .catch(err => {
        this.router.navigateByUrl('');
      });
  }
}
