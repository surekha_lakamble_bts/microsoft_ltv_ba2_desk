import { OnInit, OnDestroy, HostListener } from '@angular/core';

export class PageView implements OnInit, OnDestroy {

    protected defs1: any;
    protected defs2: any;
    protected chartOptions1 = {};
    protected chartOptions2 = {};
    protected showChart: boolean = true;

    constructor() { }

    ngOnInit() { }

    initailiseChartData() {
        this.defs1 = {
            gradient1: {
                tagName: 'linearGradient',
                id: 'gradient-1',
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1,
                children: [{
                    tagName: 'stop',
                    offset: 0
                }, {
                    tagName: 'stop',
                    offset: 1
                }]
            },
            gradient2: {
                tagName: 'linearGradient',
                id: 'gradient-2',
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1,
                children: [{
                    tagName: 'stop',
                    offset: 0
                }, {
                    tagName: 'stop',
                    offset: 1
                }]
            }
        };

        this.defs2 = {
            gradient1: {
                tagName: 'linearGradient',
                id: 'gradient-3',
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 1,
                children: [{
                    tagName: 'stop',
                    offset: 0
                }, {
                    tagName: 'stop',
                    offset: 1
                }]
            }
        };

        this.chartOptions1 = {
            title: { text: '' },
            plotOptions: {
                bar: {
                    dataLabels: {
                        format: '{y:$0,0.00a}'
                    },
                    tooltip: {
                        pointFormat: '{point.y:$0,0.00a}'
                    }
                }
            }
        };

        this.chartOptions2 = {
            chart: {
                width: 500,
                height: 300
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        crop: false,
                        overflow: 'none',
                        format: '{y:$0,0.00a}'
                    },
                    tooltip: {
                        pointFormat: "{point.y:$0,0.00a}"
                    }
                }
            }
        };
    }

    removeSplChar(str) {
        return str.replace(/[$,%]/g, '');
    }

    @HostListener('window:orientationchange', ['$event'])
    onOrientationChange($event) {
        this.refreshChart(100); 
    }

    refreshChart(time) {
        this.showChart = false;
        setTimeout(() => {
            this.showChart = true;
        },time);
    }

    ngOnDestroy() { }
}
