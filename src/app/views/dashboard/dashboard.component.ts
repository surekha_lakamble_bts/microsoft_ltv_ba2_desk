import { Component, OnInit, Input, ViewEncapsulation, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
import * as _ from 'lodash';
import { CalcService, StorageService, CommunicatorService, NumberFormattingPipe } from '@btsdigital/ngx-isomer-core';
import { Router, RouterOutlet } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
const KEY_NUM_2 = 50; // used to enter calc debugger mode
import { routerTransition } from './animations';
import { NavigationService } from './navigation.service';

@Component({
  selector: 'ism-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.styl'],
  encapsulation: ViewEncapsulation.None,
  animations: [routerTransition()]
})

export class DashboardComponent implements OnInit {

  // @ViewChild('resetModal') private resetModalRef: ModalDirective;
  @ViewChild('calcDebuggerModal') private calcDebuggerModalRef: ModalDirective;
  @ViewChild('logOutModal') private logoutModalRef: ModalDirective;

  private showMenu: boolean = false;
  private tabStatusArray: boolean[] = [];
  private showResetModel: boolean = false;
  private routerArrays: string[] = [];
  private showCalcDebugger: boolean = false;
  private disableForwardFlag: boolean = false;
  private disableBackwardFlag: boolean = false;
  private dashboardEnumRef = DashboardEnum;
  private activeIndex: number = 0;
  private _defaultModel: any;
  private showLogoutModel: boolean = false;

  constructor(private calcService: CalcService,
    private router: Router,
    private storageService: StorageService,
    private communicatorService: CommunicatorService,
    private cdr: ChangeDetectorRef,
    private numberFormatter: NumberFormattingPipe,
    private navigationService: NavigationService) { }

  ngOnInit() {
    this.getTabDisableStatus();
    this.communicatorService.getEmitter('SCENE_COMPLETE').subscribe(res => {
      this.getTabDisableStatus();
    });
    this.routerArrays = ['/dashboard/feature', '/dashboard/marketing', '/dashboard/sales', '/dashboard/migration']
    this.checkForArrowStatus();
  }

  checkForArrowStatus() {
    let currentUrl = this.router.url;
    let presentIndex = this.routerArrays.findIndex(val => val == currentUrl);
    this.activeIndex = presentIndex;
    this.disableForwardFlag = presentIndex == DashboardEnum.LAST_URL;
    this.disableBackwardFlag = presentIndex == DashboardEnum.FIRST_URL;
  }

  onForwardClicked() {
    let nextUrl = this.processURLNavigation(DashboardEnum.FORWARD_FLAG);
    this.router.navigateByUrl(nextUrl);
  }

  processURLNavigation(backForFlag) {
    let currentUrl = this.router.url;
    let presentIndex = this.routerArrays.findIndex(val => val == currentUrl);
    this.disableForwardFlag = presentIndex + backForFlag == DashboardEnum.LAST_URL && backForFlag == DashboardEnum.FORWARD_FLAG;
    this.disableBackwardFlag = presentIndex + backForFlag == DashboardEnum.FIRST_URL && backForFlag == DashboardEnum.BACKWARD_FLAG;
    this.activeIndex = presentIndex + backForFlag;
    if (backForFlag == DashboardEnum.FORWARD_FLAG) {
      let completeStatus = Number(this.calcService.getValue('tlOutput_Scene' + (presentIndex + 1) + 'Complete'));
      if (completeStatus == DashboardEnum.INCOMPLETE_STATUS)
        this.calcService.setValue('tlOutput_Scene' + (presentIndex + 1) + 'Complete', DashboardEnum.COMPLETE_STATUS).then((res) => {
          this.communicatorService.trigger('SCENE_COMPLETE');
        });
    }
    return this.routerArrays[presentIndex + backForFlag];
  }

  onBackwardClicked() {
    let nextUrl = this.processURLNavigation(DashboardEnum.BACKWARD_FLAG);
    this.router.navigateByUrl(nextUrl);
  }

  closeMenu() {
    this.showMenu = false;
  }

  toggleMenu($event) {
    this.showMenu = !this.showMenu;
  }

  onLogoutClicked() {
    this.showLogoutModel = true;
    this.cdr.detectChanges();
    this.logoutModalRef.show();
  }

  logout() {
    this.closeMenu();
    this.router.navigate(['/logout']);
  }

  onLogoutClosedClicked() {
    this.logoutModalRef.hide();
    this.showLogoutModel = false;
  }

  resetSim() {
    let arrayToBeReset: string[] = ['tlInputARPUScen1', 'tlInputCTSpUScen1', 'tlInputChurnScen1', 'tlInputCACpUScen1', 'tlInputARPUScen2Seg1',
      'tlInputARPUScen2Seg2', 'tlInputARPUScen2Seg3', 'tlInputCTSpUScen2', 'tlInputChurnScen2', 'tlInputCACpUScen2',
      'tlInputARPUScen3', 'tlInputCTSpUScen3', 'tlInputChurnScen3', 'tlInputCACpUScen3', 'tlInputARPUScen4Seg1',
      'tlInputARPUScen4Seg2', 'tlInputARPUScen4Seg3', 'tlInputCTSpUScen4', 'tlInputChurnScen4', 'tlInputCACpUScen4'];
    this.resetInputs(arrayToBeReset, this.calcService);
  }

  resetInputs(arrRefs: Array<string>, calcServiceInstance: CalcService): Promise<any> {
    return new Promise((resolve, reject) => {
      let arrPromises = [];
      this.getPristineModel(calcServiceInstance).then((calcApi: any) => {
        arrRefs.forEach((ref, refIndex) => {
          let defaultVal = this.numberFormatter.transform(calcApi.getValue(ref), '0.0', 1);
          arrPromises.push(calcServiceInstance.setValue(ref, defaultVal));
        });
        Promise.all(arrPromises).then(() => {
          // this.onResetSimClosedClicked();
          resolve();
        });
      });
    });
  }

  getPristineModel(calcServiceInstance: CalcService): Promise<any> {
    if (this._defaultModel) {
      return Promise.resolve(this._defaultModel);
    }
    return calcServiceInstance.buildModel().then((calcApi: any) => {
      this._defaultModel = calcApi;
      return calcApi;
    });
  }

  // onResetSimClicked() {
  //   this.showResetModel = true;
  //   this.cdr.detectChanges();
  //   this.resetModalRef.show();
  // }

  // onResetSimClosedClicked() {
  //   this.resetModalRef.hide();
  //   this.showResetModel = false;
  // }

  getTabDisableStatus() {
    this.tabStatusArray = [];
    for (let i = 1; i <= 4; i++) {
      if (i == 1)
        this.tabStatusArray.push(true);
      else
        this.tabStatusArray.push(Number(this.calcService.getValue('tlOutput_Scene' + (i - 1) + 'Complete')) == DashboardEnum.COMPLETE_STATUS);
    }
  }

  getActiveURLKPI(index) {
    return this.routerArrays[index] == this.router.url;
  }

  @HostListener('document:keyup', ['$event'])
  onKeypress($event) {
    if ($event.keyCode == KEY_NUM_2 && $event.shiftKey && $event.ctrlKey) {
      this.showCalcDebugger = true;
      this.cdr.detectChanges();
      this.showCalcDebuggerModel();
    }
  }

  showCalcDebuggerModel() {
    this.calcDebuggerModalRef.show();
  }

  hideCalcDebuggerModal() {
    this.calcDebuggerModalRef.hide();
  }

  onTabClicked(index) {
    this.activeIndex = index;
    this.disableForwardFlag = index == DashboardEnum.LAST_URL;
    this.disableBackwardFlag = index == DashboardEnum.FIRST_URL;
    this.router.navigateByUrl(this.routerArrays[index]);
  }

  getRouteAnimation(outlet) {
    return this.navigationService.animationValue;
  }
}

export enum DashboardEnum {
  FIRST_URL = 0,
  LAST_URL = 3,
  FORWARD_FLAG = 1,
  BACKWARD_FLAG = -1,
  COMPLETE_STATUS = 1,
  INCOMPLETE_STATUS = 0,
  FIRST_TAB = 0,
  SECOND_TAB = 1,
  THIRD_TAB = 2,
  FOURTH_TAB = 3
}