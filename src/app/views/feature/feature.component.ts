import { Component, OnInit } from '@angular/core';
import { CalcService, CommunicatorService } from '@btsdigital/ngx-isomer-core';
import { PageView } from '../PageView/pageview';

@Component({
  selector: 'ism-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.styl']
})
export class FeatureComponent extends PageView implements OnInit {

  constructor(private calcService: CalcService,
    private communicatorService: CommunicatorService) {
    super();
  }

  ngOnInit() {
    this.initailiseChartData();
    this.refreshChart(400);
  }
}
