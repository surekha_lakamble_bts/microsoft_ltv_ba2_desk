import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalcService, CommunicatorService } from '@btsdigital/ngx-isomer-core';
import { PageView } from '../PageView/pageview';

@Component({
  selector: 'ism-marketing',
  templateUrl: './marketing.component.html',
  styleUrls: ['./marketing.component.styl']
})

export class MarketingComponent extends PageView implements OnInit {

  constructor(private calcService: CalcService,
    private communicatorService: CommunicatorService) {
    super();
  }

  ngOnInit() {
    this.initailiseChartData();
    this.refreshChart(400);
  }
}
