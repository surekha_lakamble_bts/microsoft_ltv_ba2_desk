import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalcService, CommunicatorService } from '@btsdigital/ngx-isomer-core';
import { PageView } from '../PageView/pageview';

@Component({
  selector: 'ism-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.styl']
})

export class SalesComponent extends PageView implements OnInit {

  constructor(private calcService: CalcService,
    private communicatorService: CommunicatorService) {
    super();
  }

  ngOnInit() {
    this.initailiseChartData();
    this.refreshChart(400);
  }
}
