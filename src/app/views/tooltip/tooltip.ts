import { Component, OnInit, Input, ViewChild, HostListener, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { TextService, CalcService } from '@btsdigital/ngx-isomer-core';
import { TooltipDirective } from 'ngx-bootstrap';

@Component({
    selector: 'isma-tooltip',
    templateUrl: './tooltip.html',
    styleUrls: ['./tooltip.styl']
})

export class Tooltip implements OnInit {
    @Input() tooltipPlacement: string = 'right';
    @Input() tooltipText: string = '';
    @Input() key: string = '';
 
    @ViewChild('tt') tooltipRef: TooltipDirective;

    constructor(private textEngineService: TextService, private calcService: CalcService, private _elementRef: ElementRef) { }

    ngOnInit() {
        if (this.key) {
            this.tooltipText = this.textEngineService.getText(this.key) == '' ? 'No Information' : this.textEngineService.getText(this.key);
        }
    }

    @HostListener('document:touchend', ['$event.target'])
    public onClick(targetElement) {
        if (!this.tooltipRef.isOpen) {
            return;
        }
        const clickedInside = this._elementRef.nativeElement.contains(targetElement);
        if (!clickedInside && this.tooltipRef.isOpen) {
            this.tooltipRef.hide();
        }
    }
}
