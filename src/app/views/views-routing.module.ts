import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalcModelLoadedGuard } from '@btsdigital/ngx-isomer-core';
import { DashboardComponent } from './dashboard/dashboard.component';

import { FeatureComponent } from './feature/feature.component';
import { SalesComponent } from './sales/sales.component';
import { MarketingComponent } from './marketing/marketing.component';
import { MigrationComponent } from './migration/migration.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [CalcModelLoadedGuard],
    children: [{
      component: FeatureComponent,
      path: 'feature',
      data: { num: 1 }
    }, {
      component: MarketingComponent,
      path: 'marketing',
      data: { num: 2 }
    }, {
      component: SalesComponent,
      path: 'sales',
      data: { num: 3 }
    }, {
      component: MigrationComponent,
      path: 'migration',
      data: { num: 4 }
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule { }
