import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewsRoutingModule } from './views-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IsomerCoreModule } from '@btsdigital/ngx-isomer-core';

import { SharedModule } from '../shared/shared.module';
import { ComponentsModule } from '../components/components.module';
import { ExternalModelComponent } from '../shared/external-model/external-model.component';
import { NavBarComponent } from '../custom-comp/nav-bar/nav-bar.component';
import { FormsModule } from '@angular/forms';
import { WebRTCModule, VideoCallManagerService, VideoCallStateManagerService } from '@btsdigital/pulsewebrtc';
import { SafeHtmlPipe } from '../safe-html.pipe';
import { FeatureComponent } from './feature/feature.component';
import { SalesComponent } from './sales/sales.component';
import { MarketingComponent } from './marketing/marketing.component';
import { MigrationComponent } from './migration/migration.component';
import { NavigationService } from '../../app/views/dashboard/navigation.service';
import { Tooltip } from './tooltip/tooltip';

@NgModule({
  imports: [
    CommonModule,
    IsomerCoreModule,
    ViewsRoutingModule,
    SharedModule,
    FormsModule,
    ComponentsModule,
    WebRTCModule
  ],
  declarations: [
    DashboardComponent,   
    NavBarComponent,
    ExternalModelComponent,
    SafeHtmlPipe,
    FeatureComponent,
    SalesComponent,
    MarketingComponent,
    MigrationComponent,
    Tooltip
  ],
  providers: [VideoCallManagerService, VideoCallStateManagerService, NavigationService]
})
export class ViewsModule { }
