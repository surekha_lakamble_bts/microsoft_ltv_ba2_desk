export const environment = {
  production: true,
  logging: true,
  hostname: 'isomer.btspulse.com',
  connectToPulse: true,
  CDN0LOCAL1LOCALHOST2: 0
};
