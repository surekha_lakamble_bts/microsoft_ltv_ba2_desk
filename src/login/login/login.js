(function (global) {

  var name, authCode, emailId, token;

  global.auth = function (user, pass, token) {

    var url = 'https://isomer.btspulse.com/Wizer/Authentication/CloudFrontLogin';
    if (!user)
      user = global.$('#email').val();
    if (!pass)
      pass = global.$('#pass').val()


    var user = {
      email: user,
      password: pass,
      eventTitle: 'MS_LifeTimeValue_BA2'
    }
    if (token) {
      user.token = token;
    }

    var settings = {
      data: user,
      type: 'POST',
      url: url,
      xhrFields: {
        withCredentials: true
      }
    }
    $(':input[type="submit"]').prop('disabled', true);
    $('.processing').show();
    global.$.ajax(settings)
      .then(function (resp) {
        if (resp.success) {
          for (var key in resp.cookies) {
            // .NET doesn't allow - in keynames, so _ are used instead.
            global.Cookies.set(key.replace(/_/g, '-'), resp.cookies[key])
          }
          global.$("#success").text("Authentication Success. You may proceed to the app");
          if (token) {
            location.href = location.origin;
          }
          else {
            location.reload(true);
          }
        } else {
          console.log(resp);
          global.$("#loginErrorMessage").text("Error: " + resp.errMsg);
          global.$('#loginBox').show();
        }
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
      .catch(function (err) {
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
        global.$("#loginErrorMessage").text("Execution error");
        global.$('#loginBox').show();
      })
  }

  global.authRegister = function () {
    name = global.$('#name').val();
    authCode = global.$('#authCode').val();

    if (!name || name.split(' ').join('') == "") {
      global.$("#regErrorMessage").text("Please a valid username");
      return;
    } else {
      global.$("#regErrorMessage").text("");
    }

    var user = {
      name: name,
      eventCode: authCode
    }
    var settings = {
      data: user,
      type: 'POST',
      url: '/Wizer/CloudFront/SelfRegistration',
      xhrFields: {
        withCredentials: true
      }
    }
    $(':input[type="submit"]').prop('disabled', true);
    $('.processing').show();
    global.$.ajax(settings)
      .then(function (resp) {
        if (resp.success && resp.message === "User self-registered successfully.") {
          for (var key in resp.cookies) {
            // .NET doesn't allow - in keynames, so _ are used instead.
            global.Cookies.set(key.replace(/_/g, '-'), resp.cookies[key])
          }
          global.$("#success").text("Authentication Success. You may proceed to the app")
          // location.reload(true)
          location.href = "/isomer-test"
        } else if (resp.success === false && (resp.message === "Event code exists." || resp.message === "Email Required")) {
          switchToEmail();
        } else {
          global.$("#regErrorMessage").text("Error: " + resp.message)
        }
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
      .catch(function (err) {
        global.$("#regErrorMessage").text("Execution error");
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
  }

  global.authEmail = function () {
    emailId = global.$('#emailId').val();

    var user = {
      name: name,
      eventCode: authCode,
      email: emailId
    }
    var settings = {
      data: user,
      type: 'POST',
      url: '/Wizer/CloudFront/SelfRegistration',
      xhrFields: {
        withCredentials: true
      }
    }
    $(':input[type="submit"]').prop('disabled', true);
    $('.processing').show();
    global.$.ajax(settings)
      .then(function (resp) {
        if (resp.success && resp.message === "Email sent successfully.") {
          switchToEmailSent();
        } else {
          global.$("#emailRegErrorMessage").text("Error: " + resp.message)
        }
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
      .catch(function (err) {
        global.$("#emailRegErrorMessage").text("Execution error");
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
  }

  global.authNewPassword = function () {

    password = global.$('#newPwd').val();

    if (!password || password.split(' ').join('') == "") {
      global.$("#newPwdErrorMessage").text("Please a valid password");
      return;
    } else {
      global.$("#newPwdErrorMessage").text("");
    }

    var data = {
      token: token,
      password: password
    }
    var settings = {
      data: data,
      type: 'POST',
      url: '/Wizer/CloudFront/Register',
      xhrFields: {
        withCredentials: true
      }
    }

    $(':input[type="submit"]').prop('disabled', true);
    $('.processing').show();
    global.$.ajax(settings)
      .then(function (resp) {
        if (resp.success) {
          global.$("#newPwdAuth").hide();
          global.$("#backToLogin").show();
          // global.auth(emailId, password, token);
        } else {
          global.$("#newPwdErrorMessage").text("Error: " + resp.message)
        }
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
      .catch(function (err) {
        global.$("#newPwdErrorMessage").text("Execution error");
        $(':input[type="submit"]').prop('disabled', false);
        $('.processing').hide();
      })
  }

  global.switchToLogin = function () {
    $("#auth-login")[0].reset();
    $("#form1").show();
    $("#form2").hide();
    $("#form3").hide();
    $("#form4").hide();
    $("#form5").hide();
  }

  global.switchToRegister = function () {
    $("#form1").hide();
    $("#form2").show();
    $("#form3").hide();
    $("#form4").hide();
    $("#form5").hide();
  }

  global.switchToEmail = function () {
    $("#form1").hide();
    $("#form2").hide();
    $("#form3").show();
    $("#form4").hide();
    $("#form5").hide();
  }

  global.switchToEmailSent = function () {
    $("#form1").hide();
    $("#form2").hide();
    $("#form3").hide();
    $("#form4").show();
    $("#form5").hide();
  }

  global.switchToForgotPassword = function () {
    $("#form1").hide();
    $("#form2").hide();
    $("#form3").hide();
    $("#form4").hide();
    $("#form5").show();
  }

  global.resetForm = function () {
    $("#registration")[0].reset();
  }

  function getRequests() {
    global.$('#loginBox').hide();
    var s1 = location.href.substring(1, location.href.length).split('&'),
      r = [], s2, i;
    for (i = 0; i < s1.length; i += 1) {
      s2 = s1[i].split('=');
      var key = decodeURIComponent(s2[0]);
      if (key.indexOf('?') > 0) {
        key = key.substring(key.indexOf('?') + 1, key.length);
      }
      r.push(decodeURIComponent(s2[1]));
    }
    return r;
  };

  function getQueryParams() {
    var paramString = location.href.split('?')[1];
    var params = [], paramArray = [];
    if (paramString) {
      params = paramString.split('&');
      if (params && params.length > 0) {
        for (i = 0; i < params.length; i++) {
          var paramObject = {};
          var splitted = params[i].split('=');
          paramObject[splitted[0]] = splitted[1];
          paramArray.push(paramObject);
        }
      }
    }
    return paramArray;
  };

  function showForgotPassword(params) {
    for (var i = 0; i < params.length; i++) {
      if (params[i]["forgotpwd"] === "true") {
        return true;
      }
    }
    return false;
  }

  var QueryString = getRequests();
  var QueryParams = getQueryParams();
  console.log(QueryString[0]);
  token = QueryString[0];
  var found = QueryString[0];
  if (QueryParams.length > 0) {
    global.$('#loginBox').show();
    if (showForgotPassword(QueryParams)) {
      global.switchToForgotPassword();
    }
  } else {
    if (found && found != "undefined" && found != undefined) {
      console.log('recieved call from LTI/LMS');
      auth('', '', QueryString[0]);
      console.log('finished the re-route process');
    }
    else {
      global.$('#loginBox').show();
    }
  }
}(window))
